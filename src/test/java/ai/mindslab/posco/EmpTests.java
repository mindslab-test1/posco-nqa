package ai.mindslab.posco;

import ai.mindslab.posco.model.EmpVO;
import ai.mindslab.posco.service.EmpService;
import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

@Log
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmpTests {

    @Value("${poscoExpertSch.imgPath}")
    private String path;

    @Autowired
    private EmpService empService;

    /*@Test
    public void getImage()
    {
        EmpVO empVO = empService.getEmpById("id_118");
        String imgDir = path;
        String imgName = empVO.getEmpNum();

        File path = new File(imgDir);
        File[] fileList = path.listFiles();

        try {
            if(fileList.length > 0){
                for(int i = 0; i < fileList.length; i++)
                {
                    System.out.println(fileList[i]);
                    if(fileList[i].toString().contains(imgName))
                    {
                        System.out.println("found");
                    }
                }
            }
        } catch (Exception ignored) {}

    }*/

    @Test
    public void getJobInfo()
    {
        String empId = "id_009";

        EmpVO empVO = empService.getEmpById(empId);
        String jobInfo  = empService.getEmpJobInfoById(empId);
        empVO.setJobDescription(jobInfo);
        log.info(empVO.toString());
    }
}
