package ai.mindslab.posco;

import ai.mindslab.posco.repository.NqaQuestionRepository;
import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@Log
@RunWith(SpringRunner.class)
@SpringBootTest
public class PoscoApplicationTests {

    @Autowired
    private NqaQuestionRepository nqaQuestionMapper;

    @Test
    public void getPossibleQuestionTest()
    {
        String keyword = "스테인리스";
        List<String> retList = nqaQuestionMapper.getPossibleQuestionsByKeyword(keyword);
        log.info(retList.toString());
    }

}
