package ai.mindslab.posco;

import ai.mindslab.posco.service.NqaService;
import lombok.extern.java.Log;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@Log
@RunWith(SpringRunner.class)
@SpringBootTest
public class nqaTests {

    @Autowired
    private NqaService nqaService;

    @Test
    public void nqaAnswerByIdsTest()
    {
        List<String> idList = new ArrayList<>();
        idList.add("257-1");
        idList.add("1-1");
        String ret = nqaService.getAnswerByAid(idList);
        log.info(ret);
    }
}
