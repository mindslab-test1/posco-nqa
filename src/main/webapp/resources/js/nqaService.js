
var nqaService = (function () {

    function getQuestion(phrase, callback) {

        $.ajax({
            url: "/nqa-question",
            type: "GET",
            data: { phrase: phrase },
            dataType: "JSON",
            async:false,
            success: function (resp, status, xhr) {
                console.log("nqa-question: ");
                console.log(resp);

                var questionsList = [];
                var questionMorpList = [];
                var answerIdList = [];

                if (resp.questions_.length > 0) {
                    answerIdList = resp.ids_;
                    questionsList.push(resp.questions_[0].question_);
                    questionMorpList.push(resp.questions_[0].questionMorp_);


                    // 첫번째 question이 가장 적절한 질문으로 가정
                    // 첫번째 question이의 questionMorp_와 비교하여
                    // 다른 questionMorp_를 갖는 question들을 questionsList에 추가한다
                    for (var i = 1; i < resp.questions_.length; i++) {
                        var dupQuestFlag = false;
                        for(var j=0; j<questionMorpList.length; j++){
                            if (resp.questions_[i].questionMorp_.indexOf(questionMorpList[j]) !== -1) {
                                dupQuestFlag = true;
                                break;
                            }
                        }

                        if(!dupQuestFlag){
                            questionsList.push(resp.questions_[i].question_);
                            questionMorpList.push(resp.questions_[i].questionMorp_);
                        }
                    }
                }

                console.log("answerIdList: " + answerIdList);
                console.log("questionsList: " + questionsList);

                var resultList = [questionsList, answerIdList];
                callback(resultList);
            },
            error: function (xhr, status, err) {
                console.error(err);
            },
        });
    }

    function getAnswer(phrase, callback) {
        $.ajax({
            url: "/nqa-answer",
            type: "GET",
            data: { phrase: phrase },
            dataType: "JSON",
            async:false,
            success: function (resp, status, xhr) {
                console.log("nqa-answer: ");
                console.log(resp);

                var answersList = [];

                if (resp.answerResult_.length > 0) {
                    for (var i = 0; i < resp.answerResult_.length; i++) {
                        answersList.push(resp.answerResult_[i].answer_);
                    }
                }

                callback(answersList);
            },
            error: function (xhr, status, err) {
                console.error(err);
            },
        });
    }

    function getAnswerByIds(ids, callback) {
        $.ajax({
            url: "/nqa-answer-by-ids",
            type: "POST",
            data: { ids: ids },
            traditional: true, // need for transferring array
            dataType: "JSON",
            async:false,
            success: function (resp, status, xhr) {
                console.log("getAnswerByIds: ");
                console.log(resp);

                var answersList = [];

                if (resp.answerResult_.length > 0) {
                    for (var i = 0; i < resp.answerResult_.length; i++) {
                        answersList.push(resp.answerResult_[i].answer_);
                    }
                }

                callback(answersList);
            },
            error: function (xhr, status, err) {
                console.error(err);
            },
        });
    }

    function process(phrase, callback) {
        getQuestion(phrase, function (respQ) {
            console.log("Q resp: " + respQ);

            var html = [];
            if(respQ[0].length > 0){
                html.push(respQ[0]);

                if(respQ[0].length > 1){
                    html.push("Narrow Down The Question");
                    // callback(html);
                }
                else{
                    getAnswerByIds(respQ[1], function (respA) {
                        console.log("A resp: " + respA);
                        html.push(respA);
                    });
                    // callback(html);
                }
            }
            else{
                //var html = [];
                html.push("No Matched Questions");
                html.push("Empty Question Input")
                //callback(html);
            }

            console.log("process ret: " + html);
            callback(html);
        });
    }


    return {
        getQuestion: getQuestion,
        getAnswer: getAnswer,
        getAnswerByIds: getAnswerByIds,
        process: process
    };
})();
