<%--
  Created by IntelliJ IDEA.
  User: hoseop
  Date: 20. 5. 20.
  Time: 오후 7:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="path"  value="${pageContext.request.contextPath}" />

<html>
<head>
    <script type="text/javascript" src="${path}/resources/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="${path}/resources/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${path}/resources/css/bootstrap.min.css" />
    <title>Title</title>
    <style>
        body{
            background-color: rgb(237, 240, 244);
        }
        .title-wrap {
            margin-bottom: 10px;
        }
        .title-wrap .title{
            color: rgb(94, 115, 255);
            font-weight: bold;
        }
        #phrase{
            border: 4px rgb(94, 115, 255) solid; height: 70px; border-radius: 10px
        }
        #search{
            position: absolute;top: 18px;right: 17px;z-index: 100;cursor:pointer
        }

        fieldset{
            border: 3px solid rgb(94, 115, 255);
            border-radius: 15px;
            display: none;
            padding: 10px;
            width: 49%;
            height: 80%;
        }
        legend{
            width: auto;
            padding: 10px;
            margin: 0;
        }
        pre{
            height: 94% ;
            overflow-y: auto;
            word-break: normal;
        }

        .search-box:last-child{
            border: 3px solid rgb(255, 67, 67);
            float: right;
        }

    </style>
</head>
<body>
    <header></header>
    <main>
        <div class="col-sm-10" style="margin: 2% auto">
            <div class="title-wrap"><span class="title">전문가 찾기 <span>v.${verInfo}</span></span></div>

            <div class="input-group mb-3">
                <input type="text" id="phrase" class="form-control" name="phrase" placeholder="찾으시는 전문가의 이름, 분야, 공정 등 다양한 방법으로 검색하세요"/>
                <div class="input-group-append">
                    <svg id="search" class="bi bi-search" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor">
                        <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                        <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                    </svg>
                </div>
            </div>

            <fieldset class="search-box">
                <legend>QUESTION</legend>
                <pre id="search-question"></pre>
            </fieldset>
            <fieldset class="search-box">
                <legend>ANSWER</legend>
                <pre id="search-answer" ></pre>
            </fieldset>

        </div>
    </main>
    <aside></aside>

    <script type="text/javascript" src="../../resources/js/nqaService.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#search").off("click").on("click", function (e) {
                console.log("nqa processing");
                var phrase = $("#phrase").val();
                console.log("phrase: " + phrase);
                $(".search-box").css("display", "inline-block");

                nqaService.process(phrase, function (resp) {
                    $("#search-answer").empty();
                    $("#search-question").empty();

                    console.log("process resp: ");
                    console.log(resp);

                    var questionSentence = JSON.stringify(resp[0], undefined, 4);
                    var answerSentence = JSON.stringify(resp[1], undefined, 4);
                    $("#search-question").html(questionSentence);
                    $("#search-answer").html(answerSentence);

                });

            })
        });
    </script>
</body>
</html>
