<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- Cache reset -->
    <meta http-equiv="Expires" content="Mon, 06 Jan 2016 00:00:01 GMT">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <!-- icon_favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="../../resources/images/ico_favicon_128x128.ico">
    <!-- css -->
    <link rel="stylesheet" type="text/css" href="../../resources/css/font.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/reset.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/all.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/css/poscoSearch.css">
    <title>POSCO | 전문가찾기</title>
    <!--[if lte IE 9]>
    <script src="../../resources/js/html5shiv.min.js"></script>
    <script src="../../resources/js/respond.min.js"></script>

    <div class="lyrWrap">
        <div class="lyr_bg ie9"></div>
        <div class="lyrBox">
            <div class="lyr_top">
                <h3>브라우저 업데이트 안내</h3>
            </div>
            <div class="lyr_mid">
                <div class="legacy-browser">
                    현재 사용중인 브라우저는 지원이 중단된 브라우저입니다.<br>
                    원활한 온라인 서비스를 위해 브라우저를 <a href="http://windows.microsoft.com/ko-kr/internet-explorer/ie-11-worldwide-languages" target="_blank">최신 버전</a>으로 업그레이드 해주세요.
                </div>
            </div>
            <div class="lyr_btm">
                <div class="btnBox sz_small">
                    <button class="btn_win_close">창 닫기</button>
                </div>
            </div>
        </div>
    </div>
    <![endif]-->
</head>

<body>
<!-- .page loading -->
<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong>&nbsp;</strong>
            <b>&nbsp;</b>
        </em>
    </span>
</div>
<!-- //.page loading -->

<!-- #wrap -->
<div id="wrap">
    <!-- #header -->
    <div id="header">
        <h1><a href="index.html">전문가찾기</a></h1>
        <div class="srchBox">
            <input type="text" class="ipt_txt" placeholder="전문가의 이름, 분야, 공정 등 다양한 방법으로 검색하세요."  autocomplete="off">
            <button type="button" class="btn_search">검색</button>
            <div class="autoComplete">
                <dl class="groupBox">
                    <dt>전문가</dt>
                    <dd>
                        <ul class="lst_people">
                            <li>
                                <em class="fl">
                                    <span class="thumb"><img src="../../resources/images/sample/img_people01.png" alt="사진"></span>
                                    <span class="text">홍길자</span>
                                    <span class="assign">(기술연구원 AI 연구그룹/수석연구원)</span>
                                </em>
                                <em class="fr">
                                    <span class="category">사물인터넷,IOT</span>
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="thumb"><img src="../../resources/images/sample/img_people02.png" alt="사진"></span>
                                    <span class="text">홍길동</span>
                                    <span class="assign">(기술연구원 AI 연구그룹/수석연구원)</span>
                                </em>
                                <em class="fr">
                                    <span class="category">사물인터넷,IOT</span>
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="thumb"><img src="../../resources/images/sample/img_people03.png" alt="사진"></span>
                                    <span class="text">홍연지</span>
                                    <span class="assign">(기술연구원 AI 연구그룹/수석연구원)</span>
                                </em>
                                <em class="fr">
                                    <span class="category">사물인터넷,IOT</span>
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="thumb"><img src="../../resources/images/sample/img_people04.png" alt="사진"></span>
                                    <span class="text">홍수연</span>
                                    <span class="assign">(기술연구원 AI 연구그룹/수석연구원)</span>
                                </em>
                                <em class="fr">
                                    <span class="category">사물인터넷,IOT</span>
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="thumb"><img src="../../resources/images/sample/img_people05.png" alt="사진"></span>
                                    <span class="text">홍현철</span>
                                    <span class="assign">(기술연구원 AI 연구그룹/수석연구원)</span>
                                </em>
                                <em class="fr">
                                    <span class="category">사물인터넷,IOT</span>
                                    <button type="button">선택</button>
                                </em>
                            </li>
                        </ul>
                    </dd>
                </dl>
                <dl class="groupBox">
                    <dt>기술분류</dt>
                    <dd>
                        <ul class="lst_question">
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연 BP재 주석도금원판 T1</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연 CR재 건축구조용 건축비계용강관</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연 CR재 내식강 phosporous</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연 CR재 드럼용강판 인산염처리</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연 FH재 도금용FH 알루미늄</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연공정 냉각 코일&amp;냉각장치</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연공정 모델 압연</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연공정 모델 압연하중</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연공정 모델 압하율</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연공정 치수 소성계수</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                            <li>
                                <em class="fl">
                                    <span class="category">제품</span>
                                    <span class="text">냉연공정 전기강판공정 무방향성전기강판</span>
                                </em>
                                <em class="fr">
                                    <button type="button">선택</button>
                                </em>
                            </li>
                        </ul>
                    </dd>
                </dl>
            </div>
        </div>
    </div>
    <!-- #header -->
    <!-- #container -->
    <div id="container">
        <!-- #contents -->
        <div id="contents">
            <!-- 검색 tip -->
            <div id="srchTip" class="stn">
                <div class="stn_tit">
                    <h3 class="ft_posco_blue">검색 Tip</h3>
                    <a class="btn_bgBox" href="#none">기술분류 Map<span class="fas fa-arrow-right"></span></a>
                </div>
                <div class="stn_cont">
                    <ul class="lst_srchSample">
                        <li>
                            <dl>
                                <dt>기술 분류 Map을 참고하여 궁금한 점을 질문하시면 보다 정확한 전문가를 찾을 수 있습니다.</dt>
                                <dd><a href="#none"><span class="ft_posco_lightBlue">열연공정</span>의 <span class="ft_posco_lightBlue">가열로</span> 분야 전문가를 찾아 주세요.</a></dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>궁금한 점을 기술공정과 함께 질문하면 보다 정확한 전문가를 찾을 수 있습니다.</dt>
                                <dd><a href="#none"><span class="ft_posco_lightBlue">연주공정</span>의 <span class="ft_posco_lightBlue">슬라브 Bending</span> 완화 방법에 대한 전문가를 찾아주세요.</a></dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>기술 분류 Map을 참고하여 궁금한 점을 질문하시면 보다 정확한 전문가를 찾을 수 있습니다.</dt>
                                <dd><a href="#none"><span class="ft_posco_lightBlue">Ti</span>, <span class="ft_posco_lightBlue">N 제어</span> 관련 전문가를 찾아주세요.</a></dd>
                            </dl>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- //검색 tip -->
            <!-- 추가검색 -->
            <div id="detailedSearch" class="stn">
                <div class="talkBox">
                    <div class="fl">
                        <div class="stn_tit">
                            <h3>(   )님의 질문</h3>
                        </div>
                        <div class="stn_cont">
                            <ul class="lst">

                            </ul>
                            <div class="iptBox">
                                <input type="text" class="ipt_txt">
                                <button type="button" disabled>질문하기</button>
                            </div>
                        </div>
                    </div>
                    <div class="fr">
                        <div class="stn_tit">
                            <h3>AI 답변</h3>
                        </div>
                        <div class="stn_cont">
                            <ul class="lst">

                            </ul>
                        </div>
                    </div>
                    <button type="button" class="btn_talkBox_reset"><em>초기화</em></button>
                </div>
            </div>
            <!-- //추가검색 -->
            <!-- 검색결과 -->
            <div class="srchResult">
                <div class="lot_c">
                    <div class="srchResultTxt">총 <em class="ft_posco_lightBlue">5</em>명의 전문가가 검색 되었습니다.</div>
                    <ul class="lst_srchResult">
                        <li>
                            <div class="thumb"><img src="../../resources/images/sample/img_people01.png" alt=""></div>
                            <div class="infoBox">
                                <span class="name">
                                    <strong>홍기숙</strong>(수석연구원)
                                    <a href="#none">질문하기</a>
                                </span>
                                <dl class="dlBox">
                                    <dt>소속</dt>
                                    <dd>기술연구원 AI연구 그룹</dd>
                                    <dt>분야</dt>
                                    <dd>열전달, 수치해석</dd>
                                </dl>
                                <dl class="dlBox">
                                    <dt>전화</dt>
                                    <dd><a class="link" href="#none">010-1234-5678</a></dd>
                                    <dt>전화</dt>
                                    <dd><a class="link" href="#none">02-1234-5678</a></dd>
                                </dl>
                                <dl class="dlBox">
                                    <dt>메일</dt>
                                    <dd><a class="link" href="mailto:asd123@posco.com">asd123@posco.com</a></dd>
                                </dl>
                            </div>
                            <div class="tabUi">
                                <ul class="tab_nav">
                                    <li><a class="active" href="#none">전문분야</a></li>
                                    <li><a href="#none">답변사례</a></li>
                                </ul>
                                <div class="tab_container">
                                    <div class="tab_content">
                                        <ul class="lst">
                                            <li>oooo ooooooo ooo oo o (사람찾기 업무담당 정보)</li>
                                            <li>oooo ooooooo ooo oo o (연구원에게 받은 전문분야 정보)</li>
                                        </ul>
                                    </div>
                                    <div class="tab_content">
                                        <div class="qnaBox">
                                            <div class="qBox">철강 공업에서 냉연과 열연의 차이가 무엇인가요? 또한 열연, 냉연, 압연, 절연등의 용어와 그 가치가 무엇인지 알고 싶습니다.</div>
                                            <div class="aBox">일반적으로 철강은 열연에 의해서 처음 생산이 됩니다. 코우크스와 고철등을 이용해서 융점(melting point)을 저감해서 양산을 하기 위한 방법이지요!

                                                이런 열연제품은 통상적으로 불순물이나 기공등을 많이 내포하고 있어서 제 2가공업체 또는 포항제철 내 조강공장에서 다시 냉연강판으로 제 가공하는 형태로 시중에 판매 되어 지는 과정이 일반적 철강의 제조공정입니다.

                                                열연은 조강과정에서 가열을 가미한 형태이며 냉연은 상온에서 가공한 형태가 가장 간단한 설명이며, 압연은 열연과정에서 불순물량이나, 기포등을 제거하기 위한 과정과 두께를 줄이기 위한 방법입니다.

                                                냉연처리를 함으로서 부가가치가 높아진다는 의미는 조강을 함으로서 품질이 좋아 질 뿐만 아니라 틈새시장의 공략도 우수하기 때문입니다.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- //검색결과 -->
        </div>
        <!-- //#contents -->
    </div>
    <!-- //#container -->
</div>
<!-- //#wrap -->

<!-- 공통 script -->
<script type="text/javascript" src="../../resources/js/jquery-1.12.3.min.js"></script>
<!-- Font Awesome -->
<script type="text/javascript" src="../../resources/js/all.js"></script>
<!-- NQA script -->
<script type="text/javascript" src="../../resources/js/nqaService.js"></script>
<!-- page Landing -->
<script type="text/javascript">
    $(window).load(function() {
        //page loading delete
        $('#pageldg').addClass('pageldg_hide').delay(300).queue(function() { $(this).remove(); });
    });
</script>

<!-- script -->
<script type="text/javascript">
    jQuery.event.add(window,"load",function(){
        $(document).ready(function (){
            //커서 이동
            $('.srchBox .ipt_txt').focus();

            //엔터키 검색
            $('.srchBox .ipt_txt').keypress(function(e) {
                if ( event.which == 13 ) {
                    $('.srchBox .btn_search').click();
                    return false;
                }
            });

            //자동완성 show
            $('.srchBox .ipt_txt').on('keyup', function(){
                var srchTxt = $(this).val(),
                    srchTxtLength = $(this).val().length,
                    autoCompleteTxt = $('.autoComplete dl.groupBox span.text').text();

                $('.autoComplete').removeClass('on');
                $('.autoComplete dl.groupBox dd ul li').css('display','none');
                $('.autoComplete dl.groupBox').css('display','none');

                if (srchTxtLength > 1) {
                    $('.autoComplete').addClass('on');

                    //검색 단어 show
                    $('.autoComplete dl.groupBox span.text:contains("'+srchTxt+'")').each(function () {
                        var regex = new RegExp(srchTxt);

                        $(this).html(
                            $(this).text().replace(regex, '<strong class="ft_posco_lightBlue">'+srchTxt+'</strong>')
                        );
                        $(this).parents('li').css('display','block');
                        $(this).parents('dl.groupBox').css('display','block');
                    });
                }
            });

            //자동완성 선택
            $('.autoComplete dl.groupBox dd ul li').on('click',function(){
                var selectTxt = $(this).find('span.text').text();

                //선택된 검색어 입력
                $('.srchBox .ipt_txt').val('');
                $('.srchBox .ipt_txt').val(selectTxt);

                //autoComplete 초기화
                $('.autoComplete').removeClass('on');
                $('.autoComplete dl.groupBox dd ul li').css('display','none');
                $('.autoComplete dl.groupBox').css('display','none');
            });

            //검색Tip 클릭
            $('.lst_srchSample li dl dd a').on('click',function(){
                var srchSampleTxt = $(this).text();

                //선택된 검색어 입력
                $('.srchBox .ipt_txt').val('');
                $('.srchBox .ipt_txt').val(srchSampleTxt);
            });

            //검색
            $('.srchBox .btn_search').on('click',function(){
                $('#wrap').addClass('transform');

                var srchBoxTxt = $('.srchBox .ipt_txt').val();

                $('.talkBox .fl ul.lst').append(' \
                <li> \
                    <span class="name"><em>홍길동</em></span> \
                    <span class="txt">'+srchBoxTxt+'</span> \
                </li>'
                );
                $('.talkBox .stn_cont .iptBox .ipt_txt').val('');
                $('.talkBox .stn_cont .iptBox .ipt_txt').focus();

                $('.talkBox .fr ul.lst').append('\
                <li> \
                    <span class="thumb">AI</span> \
                    <span class="chatLoading">\
                        <em class="chatLoading_item01"></em>\
                        <em class="chatLoading_item02"></em>\
                        <em class="chatLoading_item03"></em>\
                    </span>\
                </li>'
                );

                nqa(srchBoxTxt, function (data) {
                    setTimeout(function() {
                        $('.chatLoading').parent('li').remove();
                        $('.talkBox .fr ul.lst').append(' \
                        <li> \
                            <span class="thumb">AI</span> \
                            <span class="txt">' + data + '</span> \
                        </li>'
                        );

                    },1000);
                    $('.talkBox .fr .stn_cont').scrollTop($('.talkBox .fr .stn_cont')[0].scrollHeight);
                })

            });

            //AI대화
            $('.talkBox .stn_cont .iptBox .ipt_txt').on('focus',function(){
                $(this).parent().addClass('active');
            });

            $('.talkBox .stn_cont .iptBox .ipt_txt').on('keyup', function(){
                var userQuestionLength = $(this).val().length;

                if ( userQuestionLength > 1 ) {
                    $('.talkBox .stn_cont .iptBox button').prop('disabled', false);
                } else {
                    $('.talkBox .stn_cont .iptBox button').prop('disabled', true);
                }
            });
            $('.talkBox .stn_cont .iptBox .ipt_txt').keypress(function(e) {
                if ( event.which == 13 ) {
                    $('.talkBox .stn_cont .iptBox button').click();
                    return false;
                }
            });

            $('.talkBox .stn_cont .iptBox button').on('click',function(){
                var userQuestionTxt = $('.talkBox .stn_cont .iptBox .ipt_txt').val(),
                    lstHeight = $('.talkBox .fl ul.lst').height();

                $('.talkBox .fl ul.lst').append(' \
                <li> \
                    <span class="name"><em>홍길동</em></span> \
                    <span class="txt">'+userQuestionTxt+'</span> \
                </li>'
                );
                $('.talkBox .stn_cont .iptBox .ipt_txt').val('');
                $('.talkBox .fl ul.lst').scrollTop($('.talkBox .fl ul.lst')[0].scrollHeight);

                if ( lstHeight > 170 ) {
                    $('.talkBox .fl ul.lst').css({
                        overflow: 'auto',
                    });
                } else {
                    $('.talkBox .fl ul.lst').css({
                        overflow: 'hidden',
                    });
                }

                $('.talkBox .fr ul.lst').append('\
                <li> \
                    <span class="thumb">AI</span> \
                    <span class="chatLoading">\
                        <em class="chatLoading_item01"></em>\
                        <em class="chatLoading_item02"></em>\
                        <em class="chatLoading_item03"></em>\
                    </span>\
                </li>'
                );

                nqa(userQuestionTxt, function (data) {

                    setTimeout(function() {
                        $('.chatLoading').parent('li').remove();
                        $('.talkBox .fr ul.lst').append(' \
                        <li> \
                            <span class="thumb">AI</span> \
                            <span class="txt">' + data + '</span> \
                        </li>'
                        );
                        $('.talkBox .fr .stn_cont').scrollTop($('.talkBox .fr .stn_cont')[0].scrollHeight);
                    },1000);
                })
            });

            //대화 초기화
            $('.btn_talkBox_reset').on('click', function(){
                $('#detailedSearch .talkBox .stn_cont .lst li').remove();
                $('#detailedSearch .talkBox .stn_cont .iptBox .ipt_txt').focus();
                $('.talkBox .stn_cont .iptBox').css({
                    display: 'block',
                });
            });

            function nqa(phrase, callback) {
                console.log("nqa processing");

                nqaService.process(phrase, function (data) {
                    if(data[0] === "No Matched Questions"){
                        $('.talkBox .stn_cont .iptBox').css({
                            display: 'none',
                        });
                        callback("등록되어 있지 않습니다. 초기화를 통해 다시 시도해 주세요.");
                    }
                    else if(data[1] === "Narrow Down The Question"){
                        try {
                            data[0] = data[0].join(' 전문가를 찾으시나요?<br>');
                            data[0] = data[0].concat(' 전문가를 찾으시나요?');
                        } catch (ignored) {}
                        callback(data[0]);
                    }
                    else{
                        console.log("nqa response: ");
                        console.log(data);

                        var resultLenght = data[1].length;
                        setResultHtml(data[1]);
                        setTimeout(function () {
                            $('.ft_posco_lightBlue').html(resultLenght);
                            $('.srchResult').css({
                                display: 'block',
                            });
                            $('.talkBox .stn_cont .iptBox').css({
                                display: 'none',
                            });
                        }, 1000);

                        data = '전문가를 모두 찾았습니다.<br><br> 다시 검색을 원하신다면 하단의 초기화 버튼을 클릭해주세요'
                        callback(data);
                    }
                });
            }

        });
    });

    function setResultHtml(resultData){
        console.log(resultData);
        var innerHtml = '';

        if (resultData.length === 0)
            return;
        else{

            resultData.forEach(function (data, index) {
                $.ajax({
                    url: "/getEmpInfo",
                    type: "GET",
                    data: {
                        emp_id : data.substr(data.indexOf("id_"), 6),
                        result : data
                    },
                    dataType: "JSON",
                    async:false,
                    success: function (resp, status, xhr) {
                        var layerArr = [resp.layers.layer1 , resp.layers.layer2 , resp.layers.layer3
                                      , resp.layers.layer4 , resp.layers.layer5 , resp.layers.layer6];
                        var filtered = layerArr.filter(function (factor) {      // null 제거
                            return factor != null;
                        })

                        var div = resp.job;                                     // [ ] 괄호 안에 내용 추출
                        div = div.match(/\[.*\]/gi);
                        div += "";
                        div = div.split("[").join("");
                        div = div.split("]").join("");


                        innerHtml += '<li>' +
                            '              <div class="thumb"><img src="/getEmpImg?id=' + data.substr(data.indexOf("id_"), 6) + '" alt=""></div>' +
                            '              <div class="infoBox">' +
                            '                  <span class="name">' +
                            '                      <strong>' + resp.emp.empName + '</strong> (수석연구원)' +
                            '                      <a href="#none">질문하기</a>' +
                            '                  </span>' +
                            '                  <dl class="dlBox">' +
                            '                      <dt>소속</dt>' +
                            '                      <dd>기술연구원 AI연구 그룹</dd>' +
                            '                      <dt>분야</dt>' +
                            '                      <dd>' + div +'</dd>' +
                            '                  </dl>' +
                            '                  <dl class="dlBox">' +
                            '                      <dt>전화</dt>' +
                            '                      <dd><a class="link" href="#none">010-1234-5678</a></dd>' +
                            '                      <dt>전화</dt>' +
                            '                      <dd><a class="link" href="#none">02-1234-5678</a></dd>' +
                            '                  </dl>' +
                            '                  <dl class="dlBox">' +
                            '                      <dt>메일</dt>' +
                            '                      <dd><a class="link" href="mailto:asd123@posco.com">asd123@posco.com</a></dd>' +
                            '                  </dl>' +
                            '              </div>' +
                            '              <div class="tabUi">' +
                            '                  <ul class="tab_nav">' +
                            '                      <li><a class="active" href="#none">전문분야</a></li>' +
                            '                      <li><a href="#none">답변사례</a></li>' +
                            '                  </ul>' +
                            '                  <div class="tab_container">' +
                            '                      <div class="tab_content">' +
                            '                          <ul class="lst">' +
                            '                              <li>' + filtered.join(", ") + '</li>' +
                            '                              <li>' + resp.job + '</li>' +
                            '                          </ul>' +
                            '                      </div>' +
                            '                      <div class="tab_content">' +
                            '                          <div class="qnaBox">' +
                            '                              <div class="qBox">철강 공업에서 냉연과 열연의 차이가 무엇인가요? 또한 열연, 냉연, 압연, 절연등의 용어와 그 가치가 무엇인지 알고 싶습니다.</div>' +
                            '                              <div class="aBox">일반적으로 철강은 열연에 의해서 처음 생산이 됩니다. 코우크스와 고철등을 이용해서 융점(melting point)을 저감해서 양산을 하기 위한 방법이지요!' +
                            '                                  이런 열연제품은 통상적으로 불순물이나 기공등을 많이 내포하고 있어서 제 2가공업체 또는 포항제철 내 조강공장에서 다시 냉연강판으로 제 가공하는 형태로 시중에 판매 되어 지는 과정이 일반적 철강의 제조공정입니다.' +
                            '                                  열연은 조강과정에서 가열을 가미한 형태이며 냉연은 상온에서 가공한 형태가 가장 간단한 설명이며, 압연은 열연과정에서 불순물량이나, 기포등을 제거하기 위한 과정과 두께를 줄이기 위한 방법입니다.' +
                            '                                  냉연처리를 함으로서 부가가치가 높아진다는 의미는 조강을 함으로서 품질이 좋아 질 뿐만 아니라 틈새시장의 공략도 우수하기 때문입니다.</div>' +
                            '                          </div>' +
                            '                      </div>' +
                            '                  </div>' +
                            '              </div>' +
                            '            </li>';
                    },
                    error: function (xhr, status, err) {
                    },
                });


            })
        }

        $('.lst_srchResult').html(innerHtml);

        // tab
        $('.tabUi').each(function(){
            $('.tab_content').hide(); //Hide all content
            $('.tabUi .tab_nav ul li:first-child a').addClass('active').show(); //Activate first tab
            $('.tab_content:first-child').show(); //Show first tab content
        });
        //TAB On Click Event
        $('.tabUi .tab_nav li a').on('click', function(){

            $(this).parents('ul.tab_nav').find('a').removeClass('active'); //Remove any 'active' class
            $(this).addClass('active'); //Add 'active' class to selected tab
            $(this).parents('.tabUi').find('.tab_content').fadeOut(200); //Hide all tab content

            var activeLength = $(this).parent().index(); //Find the href attribute value to identify the active tab + content

            $(this).parents('.tabUi').find('.tab_content').eq(activeLength).delay(200).fadeIn(); //Fade in the active ID content

            return false;
        });

        // 검색결과 더보기
        if (resultData.length > 3) {
            if (!$(".btn_more").length >= 1){
                $('.lst_srchResult > li').parents('.lot_c').append('<button type="button" class="btn_more">더보기</button>');
            }
        }
        else {
            $('.btn_more').remove();
        }


        $('.lst_srchResult > li').each(function(){
            var srchResultIndex = $(this).index();

            if ( srchResultIndex < 3 ) {
                $('.lst_srchResult li').css({
                    display: 'block',
                });
            } else {
                $('.lst_srchResult li').css({
                    display: 'none',
                });
                $('.lst_srchResult li:nth-child(1), .lst_srchResult li:nth-child(2), .lst_srchResult li:nth-child(3)').css({
                    display: 'block',
                });
            }

            $('.btn_more').on('click', function(){
                $('.lst_srchResult li').css({
                    display: 'block',
                });
                $(this).remove();
            });
        });
    }
</script>
</body>
</html>

