package ai.mindslab.posco.model;

import lombok.Data;

@Data
public class EmpVO {
    private String empNum;
    private String empName;
    private String empId;
    private String jobDescription;
}
