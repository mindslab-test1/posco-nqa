package ai.mindslab.posco.model;

import lombok.Data;

@Data
public class AnswerVO {
    private Integer id;
    private Integer copyId;
    private String answer;
    private String answerView;
    private String layer1;
    private String layer2;
    private String layer3;
    private String layer4;
    private String layer5;
    private String layer6;
}
