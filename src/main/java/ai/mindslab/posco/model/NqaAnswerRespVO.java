package ai.mindslab.posco.model;

import lombok.Data;
import maum.brain.qa.nqa.Nqa;

@Data
public class NqaAnswerRespVO {
    private String id;                  // id
    private String src;                 // 출처
    private String channel;             // 채널
    private String category;            // 카테고리
    private float score;                // 점수
    private String answer;              // 답변
    private String answerView;          // code포함 답변
    private String summary;             // ?
    private String ner;                 // 개체명
    private Nqa.Attribute attributes;   // 속성
    private Nqa.Layer layers;           // 계층
    private String tags;                // 태그
}
