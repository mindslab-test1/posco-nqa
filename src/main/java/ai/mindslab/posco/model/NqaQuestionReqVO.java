package ai.mindslab.posco.model;

import lombok.Data;
import maum.brain.qa.nqa.Nqa;

import java.util.HashMap;
import java.util.Map;

@Data
public class NqaQuestionReqVO {
    private int ntop;                               // 답변 갯수
    private String id;                              // id

    private String src;                             // 출처
    private String category;                        // 카테고리
    private float score;                            // 점수 필터
    private float maxSize;                          // 사이트 필터
    private float mm;                               // 최소 매치
    private Nqa.BertCfg bert;                       // bert 사용 여부
    private Nqa.QueryType queryType;                // 쿼리 타입 (ALL, AND, OR, Answer)
    private Nqa.QueryTarget queryTarget;            // 쿼리 타겟

    private Nqa.QueryUnit userQuery;                // 입력한 질문
    private Nqa.AttributeQueryUnit attributes;      // 쿼리에 사용할 속성
    private Nqa.QueryUnit ner;                      // 개체명
    private Nqa.Filter filter;                      // 검색에서 제외할 값들
    private Nqa.QueryUnit tags;                     // 태그값
    private Map<String, String> meta = new HashMap<>(); // 기타 메타값
}
