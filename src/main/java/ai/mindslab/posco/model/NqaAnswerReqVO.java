package ai.mindslab.posco.model;

import lombok.Data;
import maum.brain.qa.nqa.Nqa;

import java.util.HashMap;
import java.util.Map;

@Data
public class NqaAnswerReqVO {
    private int ntop;                       // 가져올 답변의 결과 갯수
    private String ids;                     // 질문 검색 결과 나온 답변 id들

    private String src;                     // 출처
    private String channel;                 // 채널
    private String category;                // 카테고리
    private Nqa.QueryUnit userQuery;        // 사용자 질문
    private float mm;                       // 최소 매치
    private Nqa.BertCfg bert;               // bert 사용 여부

    private Nqa.QueryType queryType;        // 쿼리 타입(ALL, AND, OR)
    private Nqa.QueryTarget queryTarget;    //  쿼리 타겟


    private Nqa.AttributeQueryUnit attribute;       // 쿼리에 사용할 속성
    private Nqa.LayerQueryUnit layer;               // 계층
    private Nqa.QueryUnit ner;                      // 개체명
    private Nqa.Filter filter;                      // 검색에서 제외할 값들
    private Nqa.QueryUnit tags;                     // 태그값

    private Map<String, String> meta = new HashMap<>(); // 기타 메타값
}
