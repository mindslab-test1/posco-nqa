package ai.mindslab.posco.model;

import lombok.Data;
import maum.brain.qa.nqa.Nqa;

@Data
public class NqaQuestionRespVO {
    private String id;                  // 질문 id
    private String answerId;            // 해당하는 답변 야
    private String src;                 // 출처
    private String channel;             // 채널
    private String category;            // 카테고리
    private String question;            // 질문
    private String questionMorp;        // 질문 형태소 분셕 결과
    private float score;                // 점수
    private String ner;                 // 개체명
    private Nqa.Attribute attribute;    // 속성
}
