package ai.mindslab.posco.controller;

import ai.mindslab.posco.model.AnswerVO;
import ai.mindslab.posco.model.EmpVO;
import ai.mindslab.posco.service.AnswerService;
import ai.mindslab.posco.service.EmpService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;

@Log
@RestController
public class EmpController {

    @Value("${poscoExpertSch.imgPath}")
    private String path;

    private EmpService empService;
    private AnswerService answerService;

    @Autowired
    public EmpController(EmpService empService, AnswerService answerService)
    {
        this.empService = empService;
        this.answerService = answerService;
    }

    @GetMapping("getEmpImg")
    public void getEmpImg(
            HttpServletRequest req
            , HttpServletResponse res
            , @RequestParam("id") String empId)
    {
        try {
            ServletOutputStream imgOut = res.getOutputStream();
            EmpVO empVO = empService.getEmpById(empId);
            String imgDir = path;
            String imgName = empVO.getEmpNum();

            File path = new File(imgDir);
            File[] fileList = path.listFiles();

            if(fileList.length > 0){
                for(int i = 0; i < fileList.length; i++)
                {
                    if(fileList[i].toString().contains(imgName))
                    {
                        FileInputStream inputStream = new FileInputStream(fileList[i]);
                        int length;
                        byte[] buffer = new byte[10];
                        while ((length = inputStream.read(buffer)) != -1)
                            imgOut.write(buffer, 0, length);

                        return;
                    }
                }
            }
        } catch (NullPointerException ignored) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @GetMapping("getEmpInfo")
    public ModelAndView getEmpInfo(
            @RequestParam("emp_id") String empId
            , @RequestParam("result") String result)
    {
        ModelAndView model = new ModelAndView();
        model.setViewName("jsonView");

        EmpVO empVO = empService.getEmpById(empId);
        model.addObject("emp", empVO);

        String jobInfo = empService.getEmpJobInfoById(empId);
        model.addObject("job", jobInfo);

        AnswerVO answerVO = answerService.getLayersByAns(result);
        model.addObject("layers", answerVO);

        return model;
    }
}
