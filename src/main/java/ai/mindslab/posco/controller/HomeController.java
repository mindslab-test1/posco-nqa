package ai.mindslab.posco.controller;

import ai.mindslab.posco.service.NqaQuestionService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Log
@Controller
public class HomeController {

    @Value("${ver.major}")
    private String major;
    @Value("${ver.middle}")
    private String middle;

    private NqaQuestionService nqaQuestionService;

    @Autowired
    public HomeController(NqaQuestionService nqaQuestionService) {
        this.nqaQuestionService = nqaQuestionService;
    }

    @GetMapping({"/", "/index"})
    public String index(Model model)
    {
        log.info("index page open");

        return "index";
    }

    @GetMapping("/test_nqa")
    public String testNqa(Model model)
    {
        log.info("nqa test page open");

        String verInfo = major + "." + middle;
        model.addAttribute("verInfo", verInfo);

        model.addAttribute("name", "luna");
        model.addAttribute("item","archive");

        return "test_nqa";
    }
}
