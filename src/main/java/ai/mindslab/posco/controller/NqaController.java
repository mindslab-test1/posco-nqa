package ai.mindslab.posco.controller;

import ai.mindslab.posco.service.NqaService;
import ai.mindslab.posco.service.NqaServiceImpl;
import com.google.gson.Gson;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log
@RestController
public class NqaController {

    private NqaService nqaService;

    @Autowired
    public NqaController(NqaService nqaService) {
        this.nqaService = nqaService;
    }

    @GetMapping("/nqa-indexing")
    public String nqaIndexing(@RequestParam("type") String collectionType)
    {
        return nqaService.indexing(collectionType);
    }

    @GetMapping("/nqa-question")
    public String nqaQuestion(@RequestParam(name = "phrase") String param)
    {
        String phrase = param;
        return nqaService.getQuestion(phrase);
    }

    @GetMapping("/nqa-answer")
    public String nqaAnswer(@RequestParam(name = "phrase") String param)
    {
        String phrase = param;
        return nqaService.getAnswer(phrase);
    }


    @PostMapping("/nqa-answer-by-ids")
    public String nqaAnswerByAid(@RequestParam(name = "ids") List<String> ids)
    {
        log.info("nqaAnswerByAid: " + ids.toString());

        return nqaService.getAnswerByAid(ids);
    }
}
