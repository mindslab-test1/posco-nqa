package ai.mindslab.posco.repository;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface NqaQuestionRepository {
    List<String> getPossibleQuestionsByKeyword(String keyword);
}
