package ai.mindslab.posco.repository;

import ai.mindslab.posco.model.EmpVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface EmpRepository {
    EmpVO getEmpById(String empId);
    String getEmpJobInfoById(String empId);
}
