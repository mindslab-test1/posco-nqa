package ai.mindslab.posco.repository;

import ai.mindslab.posco.model.AnswerVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface AnswerRepository {
    AnswerVO getLayersByAns(String answer);
}
