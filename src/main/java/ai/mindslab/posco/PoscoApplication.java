package ai.mindslab.posco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoscoApplication {
    public static void main(String[] args) {
        SpringApplication.run(PoscoApplication.class, args);
    }

}
