package ai.mindslab.posco.service;

import ai.mindslab.posco.model.AnswerVO;
import ai.mindslab.posco.repository.AnswerRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log
@Service
@Transactional
public class AnswerServiceImpl implements AnswerService{

    private AnswerRepository answerRepository;

    @Autowired
    public AnswerServiceImpl(AnswerRepository answerRepository){
        this.answerRepository = answerRepository;
    }

    @Override
    public AnswerVO getLayersByAns(String answer) {
        return answerRepository.getLayersByAns(answer);
    }
}
