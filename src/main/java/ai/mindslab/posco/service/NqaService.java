package ai.mindslab.posco.service;

import java.util.List;

public interface NqaService {
    String getQuestion(String phrase);
    String getAnswer(String phrase);
    String getAnswerByAid(List<String> ids);
    String indexing(String collectionType);
}
