package ai.mindslab.posco.service;

import ai.mindslab.posco.nqa.*;
import lombok.extern.java.Log;
import maum.brain.qa.nqa.Admin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Log
@Service
public class NqaServiceImpl implements NqaService{

    @Value("${nqa.ip}")
    private String host;
    @Value("${nqa.port}")
    private int port;

    private NqaClient nqaClient;
    private NqaAdmin nqaAdmin;


    @Override
    public String getQuestion(String phrase)
    {
        nqaClient = new NqaClient(host, port);
        String ret = nqaClient.getQuestion(phrase);
        log.info(ret);
        nqaClient.finalize();

        return ret;
    }

    @Override
    public String getAnswer(String phrase)
    {
        nqaClient = new NqaClient(host, port);
        String ret = nqaClient.getAnswer(phrase);
        log.info(ret);
        nqaClient.finalize();

        return ret;
    }

    @Override
    public String getAnswerByAid(List<String> ids) {
        nqaClient = new NqaClient(host, port);
        String ret = nqaClient.getAnswerByAid(ids);
        log.info(ret);
        nqaClient.finalize();

        return ret;
    }

    @Override
    public String indexing(String collectionType)
    {
        Admin.CollectionType cType = null;
        if(collectionType.equals("question"))
        {
            cType = Admin.CollectionType.QUESTION;
        }
        else if(collectionType.equals("answer"))
        {
            cType = Admin.CollectionType.ANSWER;
        }
        else
        {
            cType = Admin.CollectionType.BOTH;
        }

        nqaAdmin = new NqaAdmin(host, port);
        String ret = nqaAdmin.indexing(cType);
        log.info(ret);
        nqaAdmin.finalize();

        return ret;
    }
}
