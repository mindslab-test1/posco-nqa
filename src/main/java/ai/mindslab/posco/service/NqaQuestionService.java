package ai.mindslab.posco.service;

import java.util.List;

public interface NqaQuestionService {
    List<String> getPossibleQuestionsByKeyword(String keyword);
}
