package ai.mindslab.posco.service;

import ai.mindslab.posco.model.EmpVO;

import java.util.List;

public interface EmpService {
    EmpVO getEmpById(String empId);
    String getEmpJobInfoById(String empId);
}
