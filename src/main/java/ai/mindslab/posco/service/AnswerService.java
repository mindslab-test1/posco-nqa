package ai.mindslab.posco.service;


import ai.mindslab.posco.model.AnswerVO;

public interface AnswerService {
    AnswerVO getLayersByAns(String answer);
}
