package ai.mindslab.posco.service;

import ai.mindslab.posco.repository.EmpRepository;
import ai.mindslab.posco.model.EmpVO;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log
@Service
@Transactional
public class EmpServiceImpl implements EmpService {

    private EmpRepository empMapper;

    @Autowired
    public EmpServiceImpl(EmpRepository empMapper){
        this.empMapper = empMapper;
    }

    @Override
    public EmpVO getEmpById(String param) {
        EmpVO empVO = new EmpVO();
        try {
            empVO = empMapper.getEmpById(param);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return empVO;
    }

    @Override
    public String getEmpJobInfoById(String empId)
    {
        return empMapper.getEmpJobInfoById(empId);
    }
}
