package ai.mindslab.posco.service;

import ai.mindslab.posco.repository.NqaQuestionRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log
@Service
@Transactional
public class NqaQuestionServiceImpl implements NqaQuestionService {

    private NqaQuestionRepository nqaQuestionMapper;

    @Autowired
    public NqaQuestionServiceImpl(NqaQuestionRepository nqaQuestionMapper)
    {
        this.nqaQuestionMapper = nqaQuestionMapper;
    }

    @Override
    public List<String> getPossibleQuestionsByKeyword(String keyword)
    {
        return nqaQuestionMapper.getPossibleQuestionsByKeyword(keyword);
    }
}
