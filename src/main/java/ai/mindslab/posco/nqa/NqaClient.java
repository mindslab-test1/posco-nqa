package ai.mindslab.posco.nqa;

import com.google.gson.Gson;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.java.Log;
import maum.brain.qa.nqa.NQaServiceGrpc;
import maum.brain.qa.nqa.Nqa;

import java.util.ArrayList;
import java.util.List;


@Log
public class NqaClient {

    private final ManagedChannel channel;
    private final NQaServiceGrpc.NQaServiceBlockingStub blockingStub;
    private final NQaServiceGrpc.NQaServiceStub asyncStub;

    public NqaClient(String host, int port) {
        this.channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
        this.asyncStub = NQaServiceGrpc.newStub(channel);
        this.blockingStub = NQaServiceGrpc.newBlockingStub(channel);
    }

    public void finalize()
    {
        this.channel.shutdown();
    }

    public String getAnswer(String phrase)
    {
        Nqa.QueryUnit query = Nqa.QueryUnit.newBuilder()
                .setPhrase(phrase)
                .build();

        Nqa.SearchAnswerRequest parameterA = Nqa.SearchAnswerRequest.newBuilder()
                .setNtop(5)
                .setQueryType(Nqa.QueryType.ALL)
                .setQueryTarget(Nqa.QueryTarget.A)
                .setUserQuery(query)
                .build();
        Nqa.SearchAnswerResponse resultA = blockingStub.searchAnswer(parameterA);

        String jsonResult = new Gson().toJson(resultA);
        log.info(jsonResult);

        return jsonResult;
    }

    public String getAnswerByAid(List<String> ids)
    {
        Nqa.SearchAnswerRequest parameterA = Nqa.SearchAnswerRequest.newBuilder()
                .setNtop(5)
                .addAllIds(ids)
                .setQueryTarget(Nqa.QueryTarget.AID)
                .build();
        Nqa.SearchAnswerResponse resultA = blockingStub.searchAnswer(parameterA);

        String jsonResult = new Gson().toJson(resultA);
        log.info(jsonResult);

        return jsonResult;
    }

    public String getQuestion(String phrase)
    {
        Nqa.QueryUnit query = Nqa.QueryUnit.newBuilder()
                .setPhrase(phrase)
                .build();

        Nqa.SearchQuestionRequest parameterQ = Nqa.SearchQuestionRequest.newBuilder()
                .setNtop(10)
                .setUserQuery(query)
                .setQueryType(Nqa.QueryType.ALL)
                .setQueryTarget(Nqa.QueryTarget.Q)
                .addCategory("전문분야")
                .setMm(0.0f)
                .setScore(0.0f)
                .build();
        Nqa.SearchQuestionResponse resultQ = blockingStub.searchQuestion(parameterQ);

        String jsonResult = new Gson().toJson(resultQ);
        log.info(jsonResult);

        return jsonResult;
    }
}
