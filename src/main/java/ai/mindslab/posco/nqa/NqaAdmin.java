package ai.mindslab.posco.nqa;

import com.google.gson.Gson;
import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.java.Log;
import maum.brain.qa.nqa.Admin;
import maum.brain.qa.nqa.NQaAdminServiceGrpc;


@Log
public class NqaAdmin {

    private final ManagedChannel channel;
    private final NQaAdminServiceGrpc.NQaAdminServiceStub asyncStub;
    private final NQaAdminServiceGrpc.NQaAdminServiceBlockingStub blockingStub;

    public NqaAdmin(String host, int port) {
        this.channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
        this.blockingStub = NQaAdminServiceGrpc.newBlockingStub(channel);
        this.asyncStub = NQaAdminServiceGrpc.newStub(channel);
    }

    public void finalize()
    {
        this.channel.shutdown();
    }

    public String indexing(Admin.CollectionType collectionType)
    {
        Admin.IndexingRequest indexingInput = Admin.IndexingRequest.newBuilder()
                .setCollectionType(collectionType)
                .setIndexType(Admin.IndexType.FULL)
                .build();
        Admin.IndexStatus status = blockingStub.indexing(indexingInput);

        Gson gson = new Gson();
        String result = gson.toJson(status);
        log.info(result);

        return result;
    }

    public String abortIndexing(Admin.CollectionType collectionType)
    {
        Admin.IndexingRequest indexingInput = Admin.IndexingRequest.newBuilder()
                .setCollectionType(collectionType)
                .setIndexType(Admin.IndexType.FULL)
                .build();
        Admin.IndexStatus status = blockingStub.abortIndexing(indexingInput);

        Gson gson = new Gson();
        String result = gson.toJson(status);
        log.info(result);

        return result;
    }

    public String getIndexingStatus()
    {
        Admin.IndexStatus status = blockingStub.getIndexingStatus(Empty.getDefaultInstance());

        Gson gson = new Gson();
        String result = gson.toJson(status);
        log.info(result);

        return result;
    }
}
