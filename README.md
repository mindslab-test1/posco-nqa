# Project 구조
- nqa : nqa 빌드 및 proto파일
- nqa_client : nqa 예제 파이썬 파일
- src : 포스코 전문가 찾기 시스템 java project

## src 디렉토리 구조
- main : 실제 프로젝트 연관된 파일들 위치
    - java
        - controller : 각종 controller 클래스들 위치
        - mapper : mapper interface들 위치
        - model : vo 및 dto 클래스들 위치
        - nqa : nqa api정의한 클래스들 위치
        - service : service 클래스들 위치
    - proto : nqa엔진과 인터페이스를 위한 proto정의 파일들 위치
    - resources : mapper.xml 및 spring boot 설정 파일들 위치
    - webapp : css, javascript, jsp파일들 위치
        - sources : css, javascript
        - WEB-INF/views : jsp
- test : 단위 테스트 파일들 위치
