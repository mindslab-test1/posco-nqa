# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from maum.brain.qa.nqa import nqa_pb2 as maum_dot_brain_dot_qa_dot_nqa_dot_nqa__pb2


class NQaServiceStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.SearchQuestion = channel.unary_unary(
        '/maum.brain.qa.nqa.NQaService/SearchQuestion',
        request_serializer=maum_dot_brain_dot_qa_dot_nqa_dot_nqa__pb2.SearchQuestionRequest.SerializeToString,
        response_deserializer=maum_dot_brain_dot_qa_dot_nqa_dot_nqa__pb2.SearchQuestionResponse.FromString,
        )
    self.SearchAnswer = channel.unary_unary(
        '/maum.brain.qa.nqa.NQaService/SearchAnswer',
        request_serializer=maum_dot_brain_dot_qa_dot_nqa_dot_nqa__pb2.SearchAnswerRequest.SerializeToString,
        response_deserializer=maum_dot_brain_dot_qa_dot_nqa_dot_nqa__pb2.SearchAnswerResponse.FromString,
        )


class NQaServiceServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def SearchQuestion(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SearchAnswer(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_NQaServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'SearchQuestion': grpc.unary_unary_rpc_method_handler(
          servicer.SearchQuestion,
          request_deserializer=maum_dot_brain_dot_qa_dot_nqa_dot_nqa__pb2.SearchQuestionRequest.FromString,
          response_serializer=maum_dot_brain_dot_qa_dot_nqa_dot_nqa__pb2.SearchQuestionResponse.SerializeToString,
      ),
      'SearchAnswer': grpc.unary_unary_rpc_method_handler(
          servicer.SearchAnswer,
          request_deserializer=maum_dot_brain_dot_qa_dot_nqa_dot_nqa__pb2.SearchAnswerRequest.FromString,
          response_serializer=maum_dot_brain_dot_qa_dot_nqa_dot_nqa__pb2.SearchAnswerResponse.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'maum.brain.qa.nqa.NQaService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
