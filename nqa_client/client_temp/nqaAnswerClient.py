#!/usr/bin/python
# -*- coding:utf-8 -*-

from __future__ import print_function

import grpc
import sys
import json
from datetime import datetime

from maum.brain.qa.nqa import nqa_pb2
from maum.brain.qa.nqa import nqa_pb2_grpc
from google.protobuf import json_format
import argparse

import json

def print_result(resultQA):
    jsonResult = json_format.MessageToJson(resultQA, True)
    jobj = json.loads(jsonResult)
    print(json.dumps(jobj, encoding='utf-8', ensure_ascii=False))
#    print("==========================================")
#    if len(jobj["answerResult"]) > 0:
#        for result in jobj["answerResult"]:
#	         print("==========================================")
#	         print("id: " + result["id"])
#	         print("answer: " + result["answer"])
#	         print("src: " + result["src"])
#	         print("category: " + result["category"])
#	         print("layer1: " + result["layer"]["layer1"])
#	         print("layer2: " + result["layer"]["layer2"])
#	         print("layer3: " + result["layer"]["layer3"])
#	         print("layer4: " + result["layer"]["layer4"])
#	         print("attr1: " + result["attribute"]["attr1"])
#	         print("attr2: " + result["attribute"]["attr2"])
#	         print("attr3: " + result["attribute"]["attr3"])
#	         print("attr4: " + result["attribute"]["attr4"])
#	         print("==========================================")
 


def run():
    print(datetime.now())
    channel = grpc.insecure_channel('localhost:50052')
    # channel = grpc.insecure_channel('localhost:9880')
    stub = nqa_pb2_grpc.NQaServiceStub(channel)
    query = nqa_pb2.QueryUnit()
    query.phrase = "폰뱅킹 계약 펀드"
    #parameterQA = nqa_pb2.SearchAnswerRequest(ntop=5, ids=["535-1"], query_type="ALL", query_target=2)
    parameterQA = nqa_pb2.SearchAnswerRequest(ntop=5, query_type="ALL", query_target=3, user_query=query)
    resultQA = stub.SearchAnswer(parameterQA)
    print("++++++++++++++++++++++++++++++++++++++++++++")
    jsonResult = json_format.MessageToDict(resultQA, True)
    print(jsonResult)
    #print(parameterQA)
    #resultQA = stub.SearchAnswer(parameterQA)
    print("++++++++++++++++++++++++++++++++++++++++++++")
    #print_result(resultQA=resultQA)
    print(datetime.now())


if __name__ == '__main__':
    run()
