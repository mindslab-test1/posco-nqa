#!/usr/bin/python
# -*- coding:utf-8 -*-
from __future__ import print_function

import grpc
import sys
import json
from datetime import datetime

from maum.brain.qa.nqa import admin_pb2
from maum.brain.qa.nqa import admin_pb2_grpc

from google.protobuf import json_format



def run():
    print(datetime.now())
    channel = grpc.insecure_channel('localhost:50052')
    #channel = grpc.insecure_channel('localhost:9880')
    stub = admin_pb2_grpc.NQaAdminServiceStub(channel)
    #indexingInput = admin_pb2.IndexingRequest(collectionType='QUESTION', indexType='FULL')
    indexingInput = admin_pb2.IndexingRequest(collectionType='ANSWER', indexType='FULL')
    status = stub.Indexing(indexingInput)



    #stub = admin_pb2_grpc.NQaAdminServiceStub(channel)
    #indexingInput = admin_pb2.IndexingRequest(clean=True, collectionType='ANSWER')
    #indexingInput = admin_pb2.IndexingRequest(clean=True, collectionType='QUESTION')
    #status = stub.FullIndexing(indexingInput)
    jsonResult = json_format.MessageToJson(status, True)
    jobj = json.loads(jsonResult)
    print(jobj)
    print(datetime.now())

if __name__ == '__main__':
    run()

