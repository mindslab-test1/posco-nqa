#!/usr/bin/python
# -*- coding:utf-8 -*-
from __future__ import print_function

import grpc
import sys
import json
from datetime import datetime

from maum.brain.qa.nqa import nqa_pb2
from maum.brain.qa.nqa import nqa_pb2_grpc
from google.protobuf import json_format
import argparse

import json

class SearchQuestion:
    def __init__(self):
        # self.parameterQA = basicqa_pb2.S
        # self.resultQA = basicqa_pb2.QuestionOutput()
        self.parameterQA = ""
        self.resultQA = ""
        pass

    def print_result(self, resultQA):
        jsonResult = json_format.MessageToJson(resultQA, True)
        jobj = json.loads(jsonResult)
        print(json.dumps(jobj, encoding='utf-8', ensure_ascii=False))
        print("==========================================")
        if len(jobj["questionResult"]) > 0:
            print("in_question : " + jobj["inQuestion"])
            for result in jobj["idResult"]:
                print("answer_id_list: " + result["id"])
            for result in jobj["questionResult"]:
                print("==========================================")
                print("id: " + result["id"])
                print("answer_id: " + result["answerId"])
                print("question: " + result["question"])
                print("Question morp: " + result["questionMorp"])
                print("src: " + result["src"])
                print("category: " + result["category"])
                print("attr1: " + result["attribute"]["attr1"])
                print("attr2: " + result["attribute"]["attr2"])
                print("attr3: " + result["attribute"]["attr3"])
                print("attr4: " + result["attribute"]["attr4"])
                print("score: " + str(result["score"]))
                print("==========================================")

    def simple_test(self):
        channel = grpc.insecure_channel('localhost:50052')
        # channel = grpc.insecure_channel('localhost:9880')
        stub = nqa_pb2_grpc.NQaServiceStub(channel)
        user_query = nqa_pb2.QueryUnit(phrase="오티피 무통장 주식")
        self.parameterQA = nqa_pb2.SearchQuestionRequest(ntop=10, user_query=user_query, query_type="OR", query_target=0, mm=0, score=0)

        self.resultQA = stub.SearchQuestion(self.parameterQA)
        jsonResult = json_format.MessageToDict(self.resultQA, True)
        print(jsonResult)
        #print(self.resultQA)

        #self.parameterQA = basicqa_pb2.SearchAnswerRequest(ntop=5, ids=['1','2','3'])
        #tags = basicqa_pb2.QueryUnit(phrase="태그1")
        #self.parameterQA = basicqa_pb2.SearchAnswerRequest(ntop=5, tags=[tags])
        #self.resultQA = stub.SearchAnswer(self.parameterQA)
        #print(self.resultQA)


if __name__ == '__main__':

    searhcQuestion = SearchQuestion()

    searhcQuestion.simple_test()

