import ai.maum.nqa.observer.DocumentObserver;
//import com.google.protobuf.util.JsonFormat;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceStub;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.brain.nlp.Nlp.Sentence;
import maum.common.LangOuterClass.LangCode;
import org.apache.commons.lang3.StringUtils;

public class NlpTest {

  public static void main(String[] args) {
    NlpTest  nlpTest = new NlpTest();
    List<String> list = new ArrayList<>();
    list.add("오늘 기분이안좋다");
//    list.add("안녕하세요. 1");
//    list.add("안녕하세요. 2");
//    list.add("안녕하세요.3");
//    list.add("안녕하세요.4");
//    list.add("안녕하세요.5");
//    list.add("안녕하세요.6");
//    list.add("안녕하세요.7");
//    list.add("안녕하세요.8");
//    list.add("안녕하세요.9");
//    list.add("안녕하세요.0");
//    list.add("안녕하세요.-");
//    list.add("안녕하세요.=");
//    list.add("안녕하세요123");
//    list.add("안녕하세요.415");
    nlpTest.doMultiNlp(list);
  }

  public List<String> doMultiNlp(List<String> questions) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceStub nlpStub = null;
    List<String> results = new ArrayList<>();
    try {
      nlpChannel = ManagedChannelBuilder.forAddress("192.168.0.103",
          9823).usePlaintext(true).build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newStub(nlpChannel);

      DocumentObserver documentObserver = new DocumentObserver();
      CountDownLatch finishLatch = new CountDownLatch(1);
      documentObserver.setCountDownLatch(finishLatch);
      StreamObserver<InputText> requestObserver = nlpStub.analyzeMultiple(documentObserver);
      for (String question : questions) {
        InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
            .setSplitSentence(true).setUseTokenizer(true)
            .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
            .setUseSpace(true)
            .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
        requestObserver.onNext(inputText);
      }
      requestObserver.onCompleted();

      if (!finishLatch.await(100, TimeUnit.SECONDS)) {
//        logger.error("NLP Timeout");
        return questions;
      }

      List<Document> documentList = documentObserver.getDocumentList();
      nlpChannel.shutdown();
//      for (Document document : documentList) {
//        System.out.println(JsonFormat.printer().print(document));
//      }
      return results;
    } catch (Exception e) {
      e.printStackTrace();
      return questions;
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }
}
