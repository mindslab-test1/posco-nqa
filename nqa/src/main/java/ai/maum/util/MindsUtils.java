package ai.maum.util;


import org.apache.commons.lang3.StringUtils;

public class MindsUtils {

  public static String replaceChar(String str) {
    if (StringUtils.isEmpty(str)) {
      return "";
    }
    String[] searchList = new String[]{".", "?", ",", "!"};
    String[] replaceList = new String[]{" ", " ", " ", " "};
    return StringUtils.replaceEach(str, searchList, replaceList).trim().toLowerCase();
  }

  public static String doFilter(String question) {
    if (question == null) {
      return "";
    }
    String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
    return question.replaceAll(match, " ");
  }


  }
