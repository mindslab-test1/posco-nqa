package ai.maum.nqa.index;

import lombok.Setter;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class IndexJob extends QuartzJobBean {

  @Setter
  private IndexTask indexTask;

  @Override
  protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
//    indexTask.doIndexing();
  }
}
