package ai.maum.nqa.entity;

import java.sql.Date;
import lombok.Data;
import maum.brain.qa.nqa.Admin.NQaAdminChannel;

@Data
public class Channel {
  private int id;
  private String name;
  private String creatorId;
  private String updaterId;
  private Date createDtm;
  private Date updateDtm;

  public NQaAdminChannel getProto() {
    NQaAdminChannel.Builder channelProto = NQaAdminChannel.newBuilder();

    channelProto.setId(this.id);

    if (this.name != null) {
      channelProto.setName(this.name);
    }
    if (this.creatorId != null) {
      channelProto.setCreatorId(this.creatorId);
    }
    if (this.updaterId != null) {
      channelProto.setUpdaterId(this.updaterId);
    }
    if (this.createDtm != null) {
      channelProto.setCreateDtm(this.createDtm.toString());
    }
    if (this.updateDtm != null) {
      channelProto.setUpdateDtm(this.updateDtm.toString());
    }

    return channelProto.build();
  }

  public Channel setEntityByProto(NQaAdminChannel channelProto) {

    this.id = channelProto.getId();
    this.name = channelProto.getName();
    this.creatorId = channelProto.getCreatorId();
    this.updaterId = channelProto.getUpdaterId();

    return this;
  }
}
