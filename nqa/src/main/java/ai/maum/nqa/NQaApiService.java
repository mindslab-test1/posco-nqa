package ai.maum.nqa;

import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import maum.brain.qa.nqa.ApiService.AbortIndexingRequest;
import maum.brain.qa.nqa.ApiService.IndexingQaSet;
import maum.brain.qa.nqa.ApiService.IndexingResponse;
import maum.brain.qa.nqa.NQaApiServiceGrpc.NQaApiServiceImplBase;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NQaApiService extends NQaApiServiceImplBase {

  private static Logger logger = LoggerFactory.getLogger(NQaApiService.class);

  private static ApiIndexingCore apiIndexingCore;

  private ApiDocumentIndexingHandler apiDocumentIndexingHandler;

  public NQaApiService(CloudSolrClient cloudSolrClientQuestion,
      CloudSolrClient cloudSolrClientAnswer) {
    this.apiDocumentIndexingHandler = new ApiDocumentIndexingHandler(cloudSolrClientQuestion, cloudSolrClientAnswer);
  }

  @Override
  public void indexing(IndexingQaSet indexingQaSet, StreamObserver<IndexingResponse> responseObserver) {
    logger.info("start indexing");

    try {
      indexingTask(indexingQaSet, responseObserver);
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      responseObserver.onError(e);
    }
  }

  private void indexingTask(IndexingQaSet indexingQaSet, StreamObserver<IndexingResponse> responseObserver) {
    IndexingResponse.Builder indexingResponse = IndexingResponse.newBuilder();
    try {
      // check indexing
      if (apiDocumentIndexingHandler.isIndexing()) {
        // fill indexing status
        indexingResponse.setMessage("NQA is already in Indexing");
      } else {
        apiDocumentIndexingHandler.setIndexing(true);
        indexingResponse.setMessage("NQA Indexing Start");
        apiIndexingCore = new ApiIndexingCore(indexingQaSet, apiDocumentIndexingHandler);
        apiIndexingCore.start();
        Thread.sleep(10);
      }
      fillIndexingResponse(indexingResponse);
      responseObserver.onNext(indexingResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      responseObserver.onError(e);
    }
  }

  @Override
  public void abortIndexing(AbortIndexingRequest abortIndexingRequest, StreamObserver<IndexingResponse> responseObserver) {
    logger.info("start abortIndexing");
    try {
      abortIndexingTask(abortIndexingRequest, responseObserver);
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      responseObserver.onError(e);
    }
  }

  private void abortIndexingTask(AbortIndexingRequest abortIndexingRequest, StreamObserver<IndexingResponse> responseObserver) {
    IndexingResponse.Builder indexingResponse = IndexingResponse.newBuilder().setStatus(false);
    if (apiDocumentIndexingHandler.isIndexing()) {
      // stop indexing
      apiIndexingCore.interrupt();
      apiIndexingCore.stop();
      apiDocumentIndexingHandler.setIndexing(false);
      indexingResponse.setMessage("Indexing Abort Success");
    } else {
      indexingResponse.setMessage("NQA is not Indexing");
    }
    // fill indexing status
    fillIndexingResponse(indexingResponse);
    responseObserver.onNext(indexingResponse.build());
    responseObserver.onCompleted();
  }

  @Override
  public void getIndexingStatus(Empty empty, StreamObserver<IndexingResponse> responseObserver) {
    logger.info("get indexStatus");
    try {
      getIndexingStatusTask(empty, responseObserver);
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      responseObserver.onError(e);
    }
  }

  private void getIndexingStatusTask(Empty empty, StreamObserver<IndexingResponse> responseObserver) {
    IndexingResponse.Builder indexingResponse = IndexingResponse.newBuilder().setStatus(apiDocumentIndexingHandler.isIndexing());
//     fill indexing status
    fillIndexingResponse(indexingResponse);
    if (apiDocumentIndexingHandler.isIndexing()) {
      indexingResponse.setMessage("NQA_in_Indexing");
    } else {
      indexingResponse.setMessage("NQA_Not_in_Indexing");
    }
    responseObserver.onNext(indexingResponse.build());
    responseObserver.onCompleted();
  }

  private void fillIndexingResponse(IndexingResponse.Builder indexingResponse) {
    boolean indexing = apiDocumentIndexingHandler.isIndexing();
    int total = apiDocumentIndexingHandler.getTotal();
    int fetched = apiDocumentIndexingHandler.getFetched();
    int processed = apiDocumentIndexingHandler.getProcessed();
    indexingResponse.setStatus(indexing).setTotal(total).setFetched(fetched).setProcessed(processed);
  }
}
