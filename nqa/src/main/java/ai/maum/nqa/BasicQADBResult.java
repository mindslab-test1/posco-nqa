package ai.maum.nqa;

import lombok.Data;

@Data
public class BasicQADBResult {
  private String answer;
  private int skillId;
  private String src;
  private String channel;
  private String category;
  private String subCategory;
  private String subSubCategory;
}
