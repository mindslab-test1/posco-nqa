package ai.maum.nqa;

import java.sql.Date;
import lombok.Data;

@Data
public class ParaphraseWord {
  private int id;
  private String mainWord;
  private String paraphraseWord;
  private Date createAt;
}
