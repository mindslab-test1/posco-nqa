package ai.maum.nqa;

import java.sql.Date;
import lombok.Data;

@Data
public class BasicDocument {
  private String id;
  private String indexId;
  private String answerId;
  private String answerCopyId;
  private String copyId;
  private String question;
  private String answer;
  private String answerView;
  private String summary;
  private String src;
  private String category;
  private String categoryId;
  private String channel;
  private String channelId;
  private String attr1;
  private String attr2;
  private String attr3;
  private String attr4;
  private String attr5;
  private String attr6;
  private String layer1;
  private String layer2;
  private String layer3;
  private String layer4;
  private String layer5;
  private String layer6;
  private String tags;
  private Date createDtm;
  private Date updateDtm;
}
