package ai.maum.nqa;

import ai.maum.nqa.observer.DocumentObserver;
import ai.maum.util.PropertiesManager;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import lombok.Getter;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceStub;
import maum.brain.nlp.Nlp.NamedEntity;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.Morpheme;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.brain.nlp.Nlp.Sentence;
import maum.common.LangOuterClass.LangCode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NlpService {

  private static Logger logger = LoggerFactory.getLogger(NlpService.class);

  private static String[] morpTypes = null;
  private static String[] nesTypes = null;

  private static List<String> removeMorphTypes = null;

  @Getter
  private String noun = "";

  private static Properties properties = PropertiesManager.getProperties();

  public NlpService() {
    this.morpTypes = StringUtils
        .split(properties.getProperty("MORPH_TYPES", "NNG,NNP,NNB,NP,NR,VV,VA,MM,SL,NF,SN"), ",");
    this.nesTypes = StringUtils.split(properties.getProperty("NER_TYPES", "PRD_INSV, PRD_DP, PRD_LON, PRD_FUND"),",");
    try {
      this.removeMorphTypes = Arrays.asList(StringUtils.split(new String(properties.getProperty("REMOVE_TYPES", "").getBytes("ISO-8859-1"), "UTF-8"), ","));
    } catch (Exception e) {
      e.printStackTrace();
      this.removeMorphTypes = new ArrayList<>();
    }
  }

  public String doNerNlp(String question) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceBlockingStub nlpStub = null;

    try {
      nlpChannel = ManagedChannelBuilder.forAddress(properties.getProperty("NLP_SERVER"),
          Integer.parseInt(properties.getProperty("NLP_PORT"))).usePlaintext(true).build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(nlpChannel);
      List<String> nesList = new ArrayList<>();
      InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
          .setSplitSentence(true).setUseTokenizer(true)
          .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_NAMED_ENTITY)
          .setUseSpace(StringUtils.equalsIgnoreCase("Y", properties.getProperty("SPACE_YN", "N")))
          .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
      Document document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyzeWithSpace(inputText);
//      Document document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyze(inputText);
      nlpChannel.shutdown();
      List<Sentence> sentenceList = document.getSentencesList();
      setNesSentence(nesList, sentenceList);
      String result = StringUtils.join(nesList, " ");
      return result;
    } catch (Exception e) {
//      e.printStackTrace();
      return question;
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }

  public String doMorphNlp(String question) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceBlockingStub nlpStub = null;

    try {
      nlpChannel = ManagedChannelBuilder.forAddress(properties.getProperty("NLP_SERVER"),
              Integer.parseInt(properties.getProperty("NLP_PORT"))).usePlaintext(true).build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(nlpChannel);
      List<String> strList = new ArrayList<>();
      List<String> nounList = new ArrayList<>();
      InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
              .setSplitSentence(true).setUseTokenizer(true)
              .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
              .setUseSpace(StringUtils.equalsIgnoreCase("Y", properties.getProperty("SPACE_YN", "N")))
              .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
      Document document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyzeWithSpace(inputText);
//      Document document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyze(inputText);
      nlpChannel.shutdown();
      List<Sentence> sentenceList = document.getSentencesList();
      setMorphSentence(strList, nounList, sentenceList);
      String result = StringUtils.join(strList, " ");
      noun = StringUtils.join(nounList, " ");
      return result;
    } catch (Exception e) {
//      e.printStackTrace();
      return question;
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }

  public void doNlp(String question, HashMap<String, String> nlpResult) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceBlockingStub nlpStub = null;

    try {
      nlpChannel = ManagedChannelBuilder.forAddress(properties.getProperty("NLP_SERVER"),
          Integer.parseInt(properties.getProperty("NLP_PORT"))).usePlaintext(true).build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(nlpChannel);
      List<String> strList = new ArrayList<>();
      List<String> nesList = new ArrayList<>();
      List<String> nounList = new ArrayList<>();
      InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
          .setSplitSentence(true).setUseTokenizer(true)
          .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_NAMED_ENTITY)
          .setUseSpace(StringUtils.equalsIgnoreCase("Y", properties.getProperty("SPACE_YN", "N")))
          .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
      Document document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyzeWithSpace(inputText);
//      Document document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyze(inputText);
      nlpChannel.shutdown();
      List<Sentence> sentenceList = document.getSentencesList();
      setMorphSentence(strList, nounList, sentenceList);
      setNesSentence(nesList, sentenceList);
      nlpResult.put("morph", StringUtils.join(strList, " "));
      nlpResult.put("ner", StringUtils.join(nesList, " "));
      noun = StringUtils.join(nounList, " ");
    } catch (Exception e) {
//      e.printStackTrace();
      nlpResult.put("morph", question);
      nlpResult.put("ner", "");
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }

//  public List<String> doMultiNlp(List<String> questions) {
//    ManagedChannel nlpChannel = null;
//    NaturalLanguageProcessingServiceStub nlpStub = null;
//    List<String> results = new ArrayList<>();
//    try {
//      nlpChannel = ManagedChannelBuilder.forAddress(properties.getProperty("NLP_SERVER"),
//          Integer.parseInt(properties.getProperty("NLP_PORT"))).usePlaintext(true).build();
//      nlpStub = NaturalLanguageProcessingServiceGrpc.newStub(nlpChannel);
//
//      DocumentObserver documentObserver = new DocumentObserver();
//      CountDownLatch finishLatch = new CountDownLatch(1);
//      documentObserver.setCountDownLatch(finishLatch);
//      StreamObserver<InputText> requestObserver = nlpStub.analyzeMultiple(documentObserver);
//      for (String question : questions) {
//        InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
//            .setSplitSentence(true).setUseTokenizer(true)
//            .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
//            .setUseSpace(StringUtils.equalsIgnoreCase("Y", properties.getProperty("SPACE_YN", "N")))
//            .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
//        requestObserver.onNext(inputText);
//      }
//      requestObserver.onCompleted();
//
//      if (!finishLatch.await(100, TimeUnit.SECONDS)) {
//        logger.error("NLP Timeout");
//        return questions;
//      }
//
//      List<Document> documentList = documentObserver.getDocumentList();
//      nlpChannel.shutdown();
//      for (Document document : documentList) {
//        List<String> strList = new ArrayList<>();
//        List<String> nounList = new ArrayList<>();
//        List<Sentence> sentenceList = document.getSentencesList();
//        setMorphSentence(strList, nounList, sentenceList);
//        String result = StringUtils.join(strList, " ");
//        results.add(result);
//      }
//      return results;
//    } catch (Exception e) {
//      e.printStackTrace();
//      return questions;
//    } finally {
//      if (nlpChannel != null) {
//        nlpChannel.shutdown();
//      }
//    }
//  }

  private void setMorphSentence(List<String> strList, List<String> nounList,
      List<Sentence> sentenceList) {
    for (Sentence sentence : sentenceList) {
      List<Morpheme> morpsList = sentence.getMorpsList();
      if ("Y".equalsIgnoreCase(properties.getProperty("REMOVE_MODE","N"))) {
        morpsList = removeUnnecessaryMorph(morpsList);
      }
      for (int i = 0; i < morpsList.size(); i++) {
        Morpheme morpheme = morpsList.get(i);
        if (i > 0 && ((StringUtils
            .equalsAny(morpsList.get(i - 1).getType(), "NNG", "NNP", "NNB", "NP", "NR")
            && StringUtils
            .equals(morpheme.getType(), "XSN"))
            || (StringUtils.equals(morpsList.get(i - 1).getType(), "VV") && StringUtils
            .equals(morpheme.getType(), "ETN"))
            || (StringUtils.equals(morpsList.get(i - 1).getType(), "XPN") && StringUtils
            .equalsAny(morpheme.getType(), "NNG", "NNP", "NNB", "NP", "NR")))) {
          String temp = strList.get(strList.size() - 1);
          if (strList.size() > 0) {
            strList.remove(strList.size() - 1);
          }
          strList.add(temp + morpheme.getLemma());
          if (nounList.size() > 0) {
            nounList.remove(nounList.size() - 1);
          }
          nounList.add(temp + morpheme.getLemma());
        } else if (StringUtils.equalsAny(morpheme.getType(), morpTypes)) {
          strList.add(morpheme.getLemma());
          if (!StringUtils.equalsAny(morpheme.getType(), "VV", "VA")) {
            nounList.add(morpheme.getLemma());
          }
        }
      }
    }
  }

  private List<Morpheme> removeUnnecessaryMorph(List<Morpheme> morphemeList) {
    List<Morpheme> newMorphemeList = new ArrayList<>();
    for (int i=0;i<morphemeList.size();i++) {
      if (removeMorphTypes.contains(morphemeList.get(i).getLemma() + "/" + morphemeList.get(i).getType())) {
//        logger.info("remove type => " + morphemeList.get(i).getLemma() + "/" + morphemeList.get(i).getType());
      } else {
        newMorphemeList.add(morphemeList.get(i));
      }
    }
    return newMorphemeList;
  }

  private void setNesSentence(List<String> strList, List<Sentence> sentenceList) {
    for (Sentence sentence : sentenceList) {
      for (NamedEntity namedEntity : sentence.getNesList()) {
        for (String nes : nesTypes) {
          if (namedEntity.getType().contains(nes)) {
            strList.add(namedEntity.getText());
            strList.add(namedEntity.getText() + "/" + namedEntity.getType());
            break;
          }
        }
      }
    }
  }
}
