package ai.maum.nqa;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DocumentIndexingHandler implements ResultHandler<BasicDocument> {

  private static Logger logger = LoggerFactory.getLogger(DocumentIndexingHandler.class);

  private CloudSolrClient cloudSolrClient;

  @Getter
  @Setter
  private int processed = 0;

  @Getter
  @Setter
  private int fetched = 0;

  @Getter
  @Setter
  private int total = 0;

  @Getter
  @Setter
  private boolean isIndexing = false;

  @Setter
  private Map<String, List<String>> paraMap = new HashMap<>();

  Collection<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();

  NlpService nlpService = new NlpService();

  public DocumentIndexingHandler(CloudSolrClient cloudSolrClient) {
    this.cloudSolrClient = cloudSolrClient;
  }

  public void clearValue() {
    this.processed = 0;
    this.fetched = 0;
    this.total = 0;
  }

  @Override
  public void handleResult(ResultContext<? extends BasicDocument> resultContext) {

    try {

      fetched = resultContext.getResultCount();
      BasicDocument basicDocument = resultContext.getResultObject();
      SolrInputDocument solrInputDocument = new SolrInputDocument();
      HashMap<String,String> nlpResult = new HashMap<>();
      logger.debug(basicDocument.toString());

      if (StringUtils.isNotEmpty(resultContext.getResultObject().getQuestion())) {
        solrInputDocument.addField("id", basicDocument.getId());
        solrInputDocument.addField("answer_id", basicDocument.getAnswerId() + "-" + basicDocument.getAnswerCopyId());
        solrInputDocument.addField("question", basicDocument.getQuestion());
        nlpService.doNlp(basicDocument.getQuestion(), nlpResult);
        solrInputDocument.addField("question_morph", nlpResult.get("morph"));
        solrInputDocument.addField("question_gram", nlpResult.get("morph"));
        solrInputDocument.addField("ner", nlpResult.get("ner"));
        solrInputDocument.addField("src", basicDocument.getSrc());
        solrInputDocument.addField("category", basicDocument.getCategory());
        solrInputDocument.addField("channel", basicDocument.getChannel());
        solrInputDocument.addField("attr1", basicDocument.getAttr1());
        solrInputDocument.addField("attr2", basicDocument.getAttr2());
        solrInputDocument.addField("attr3", basicDocument.getAttr3());
        solrInputDocument.addField("attr4", basicDocument.getAttr4());
        solrInputDocument.addField("attr5", basicDocument.getAttr5());
        solrInputDocument.addField("attr6", basicDocument.getAttr6());
//        nlpService.doNlp(basicDocument.getAttr1(), nlpResult);
//        solrInputDocument.addField("attr1", nlpResult.get("morph"));
//        nlpService.doNlp(basicDocument.getAttr2(), nlpResult);
//        solrInputDocument.addField("attr2", nlpResult.get("morph"));
//        nlpService.doNlp(basicDocument.getAttr3(), nlpResult);
//        solrInputDocument.addField("attr3", nlpResult.get("morph"));
//        nlpService.doNlp(basicDocument.getAttr4(), nlpResult);
//        solrInputDocument.addField("attr4", nlpResult.get("morph"));
//        nlpService.doNlp(basicDocument.getAttr5(), nlpResult);
//        solrInputDocument.addField("attr5", nlpResult.get("morph"));
//        nlpService.doNlp(basicDocument.getAttr6(), nlpResult);
//        solrInputDocument.addField("attr6", nlpResult.get("morph"));
        nlpService.doNlp(basicDocument.getTags(), nlpResult);
        solrInputDocument.addField("tags_morph", nlpResult.get("morph"));
        solrInputDocument.addField("tags_gram", nlpResult.get("morph"));
        solrInputDocument.addField("create_dtm", basicDocument.getCreateDtm());
        solrInputDocument.addField("update_dtm", basicDocument.getUpdateDtm());
      } else {
        solrInputDocument.addField("id", basicDocument.getId() + "-" + basicDocument.getCopyId());
        solrInputDocument.addField("answer", basicDocument.getAnswer());
//        String outPutAnswer = processAnswer(basicDocument);
//        solrInputDocument.addField("answer", outPutAnswer);
//        nlpService.doNlp(outPutAnswer, nlpResult);
        nlpService.doNlp(basicDocument.getAnswer(), nlpResult);
        solrInputDocument.addField("answer_morph", nlpResult.get("morph"));
        solrInputDocument.addField("answer_gram", nlpResult.get("morph"));
        solrInputDocument.addField("answer_view", basicDocument.getAnswerView());
        solrInputDocument.addField("ner", nlpResult.get("ner"));
        solrInputDocument.addField("summary", basicDocument.getSummary());
        solrInputDocument.addField("src", basicDocument.getSrc());
        solrInputDocument.addField("category", basicDocument.getCategory());
        solrInputDocument.addField("channel", basicDocument.getChannel());
        solrInputDocument.addField("attr1", basicDocument.getAttr1());
        solrInputDocument.addField("attr2", basicDocument.getAttr2());
        solrInputDocument.addField("attr3", basicDocument.getAttr3());
        solrInputDocument.addField("attr4", basicDocument.getAttr4());
        solrInputDocument.addField("attr5", basicDocument.getAttr5());
        solrInputDocument.addField("attr6", basicDocument.getAttr6());
//        nlpService.doNlp(basicDocument.getAttr1(), nlpResult);
//        solrInputDocument.addField("attr1", nlpResult.get("morph"));
//        nlpService.doNlp(basicDocument.getAttr2(), nlpResult);
//        solrInputDocument.addField("attr2", nlpResult.get("morph"));
//        nlpService.doNlp(basicDocument.getAttr3(), nlpResult);
//        solrInputDocument.addField("attr3", nlpResult.get("morph"));
//        nlpService.doNlp(basicDocument.getAttr4(), nlpResult);
//        solrInputDocument.addField("attr4", nlpResult.get("morph"));
//        nlpService.doNlp(basicDocument.getAttr5(), nlpResult);
//        solrInputDocument.addField("attr5", nlpResult.get("morph"));
//        nlpService.doNlp(basicDocument.getAttr6(), nlpResult);
//        solrInputDocument.addField("attr6", nlpResult.get("morph"));
        solrInputDocument.addField("layer1", basicDocument.getLayer1());
        solrInputDocument.addField("layer2", basicDocument.getLayer2());
        solrInputDocument.addField("layer3", basicDocument.getLayer3());
        solrInputDocument.addField("layer4", basicDocument.getLayer4());
        solrInputDocument.addField("layer5", basicDocument.getLayer5());
        solrInputDocument.addField("layer6", basicDocument.getLayer6());
        nlpService.doNlp(basicDocument.getTags(), nlpResult);
        solrInputDocument.addField("tags_morph", nlpResult.get("morph"));
        solrInputDocument.addField("tags_gram", nlpResult.get("morph"));
        solrInputDocument.addField("create_dtm", basicDocument.getCreateDtm());
        solrInputDocument.addField("update_dtm", basicDocument.getUpdateDtm());
      }

      docs.add(solrInputDocument);
      processed++;

      if (docs.size() > 0 && ((fetched % 100) == 0 || total == fetched)) {
        cloudSolrClient.add(docs);
        cloudSolrClient.commit();
        docs.clear();
      }
    } catch (SolrServerException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private List<String> doParaphrase(String question) {
    logger.info("question: {}", question);
    List<String> questionList = new ArrayList<>();
    if (StringUtils.isEmpty(question)) {
      return questionList;
    }
    Set<String> keySet = paraMap.keySet();
    int lastIndex = -1;

    // 질문의 끝에 특수문자가 있는 지 확인

    String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
    Pattern pattern = Pattern.compile(match);
    Stack<String> regxStack = new Stack<>();
    StringBuffer regxBuffer = new StringBuffer();
    for (int i = question.length() - 1; i >= 0; i--) {
      Matcher matcher = pattern.matcher(String.valueOf(question.charAt(i)));
      if (matcher.find()) {
        regxStack.push(matcher.group());
      }
    }

    for (int i = 0; i < regxStack.size(); i++) {
      regxBuffer.append(regxStack.pop());
    }

    // 질문의 특수문자 제거
    String cleanQuestion = StringUtils.substring(question, 0, question.length() - regxStack.size());

    StringBuilder questionBuilder = new StringBuilder(cleanQuestion);

    String[] orgWords = StringUtils.split(StringUtils.trim(cleanQuestion), " ");
    String lastWord = orgWords[orgWords.length - 1];

    for (String key : keySet) {
      boolean isEqual = false;
      List<String> paraphraseList = new ArrayList<>();
      paraphraseList.addAll(paraMap.get(key));
      // 질문 어미와 대표 단어가 같은 지 비교
      lastIndex = StringUtils.lastIndexOf(cleanQuestion, key);
      if (lastIndex > -1 && StringUtils.endsWith(cleanQuestion, key)) {
        for (String paraphrase : paraphraseList) {
          StringBuilder paraphraseBuilder = questionBuilder.replace(lastIndex, questionBuilder.length(), paraphrase);
          if (StringUtils.equals(cleanQuestion, paraphraseBuilder.toString())) {
            continue;
          } else {
            questionList.add(paraphraseBuilder.toString() + regxBuffer.toString());
          }
        }
      } else {
        for (String paraphrase : paraphraseList) {
          lastIndex = StringUtils.lastIndexOf(cleanQuestion, paraphrase);
          if (lastIndex > -1 && StringUtils.endsWith(cleanQuestion, paraphrase)) {
            isEqual = true;
            break;
          }
        }
        if (isEqual) {
          StringBuilder paraphraseBuilder = questionBuilder.replace(lastIndex, questionBuilder.length(), key);
          questionList.add(paraphraseBuilder.toString() + regxBuffer.toString());
          for (String paraphrase : paraphraseList) {
            if (StringUtils.equalsIgnoreCase(lastWord, paraphrase)) {
              continue;
            }
            paraphraseBuilder = questionBuilder.replace(lastIndex, questionBuilder.length(), paraphrase);
            questionList.add(paraphraseBuilder.toString() + regxBuffer.toString());
          }
        }
      }
    }
    return questionList;
  }
}
