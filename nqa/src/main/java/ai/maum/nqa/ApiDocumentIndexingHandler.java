package ai.maum.nqa;

import ai.maum.nqa.entity.Answer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.SqlSession;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApiDocumentIndexingHandler implements ResultHandler<BasicDocument> {

  private static Logger logger = LoggerFactory.getLogger(ApiDocumentIndexingHandler.class);

  private CloudSolrClient cloudSolrQClient;
  private CloudSolrClient cloudSolrAClient;

  @Getter
  @Setter
  private SqlSession sqlSession = null;

  @Getter
  @Setter
  private int processed = 0;

  @Getter
  @Setter
  private int fetched = 0;

  @Getter
  @Setter
  private int total = 0;

  @Getter
  @Setter
  private boolean isIndexing = false;

  Collection<SolrInputDocument> docsQ = new ArrayList<SolrInputDocument>();
  Collection<SolrInputDocument> docsA = new ArrayList<SolrInputDocument>();

  NlpService nlpService = new NlpService();

  public ApiDocumentIndexingHandler(CloudSolrClient cloudSolrQClient, CloudSolrClient cloudSolrAClient) {
    this.cloudSolrQClient = cloudSolrQClient;
    this.cloudSolrAClient = cloudSolrAClient;
  }

  public void clearValue() {
    this.processed = 0;
    this.fetched = 0;
    this.total = 0;
    this.docsQ.clear();
    this.docsA.clear();
  }

  @Override
  public void handleResult(ResultContext<? extends BasicDocument> resultContext) {

    try {
      fetched = resultContext.getResultCount();
      BasicDocument basicDocument = resultContext.getResultObject();
      logger.debug(basicDocument.toString());
      SolrInputDocument solrQInputDocument = new SolrInputDocument();
      SolrInputDocument solrAInputDocument = new SolrInputDocument();
      HashMap<String, String> nlpResult = new HashMap<>();


      // add delete query first time
      if (processed == 0) {
        logger.info("Fetched_count==>" + String.valueOf(fetched));
        String deleteQuery = "channel:\"" + basicDocument.getChannel() + "\" AND category:\"" + basicDocument.getCategory() + "\"";
        logger.info("Delete_Query==>" + deleteQuery);
        cloudSolrQClient.deleteByQuery(deleteQuery);
        cloudSolrAClient.deleteByQuery(deleteQuery);
      }


      // check same answer and copyId
      List<Answer> answerList = sqlSession.selectList("selectApiAnswerId", basicDocument);

      int copyId;
      String id = basicDocument.getId();
      int answerId = answerList.get(0).getId();

      for (copyId = 0; copyId < answerList.size(); copyId++) {
        // find copyId
        if (checkStringNull(basicDocument.getLayer6()).equalsIgnoreCase(checkStringNull(answerList.get(copyId).getLayer6Name())) &&
            checkStringNull(basicDocument.getLayer5()).equalsIgnoreCase(checkStringNull(answerList.get(copyId).getLayer5Name())) &&
            checkStringNull(basicDocument.getLayer4()).equalsIgnoreCase(checkStringNull(answerList.get(copyId).getLayer4Name())) &&
            checkStringNull(basicDocument.getLayer3()).equalsIgnoreCase(checkStringNull(answerList.get(copyId).getLayer3Name())) &&
            checkStringNull(basicDocument.getLayer2()).equalsIgnoreCase(checkStringNull(answerList.get(copyId).getLayer2Name())) &&
            checkStringNull(basicDocument.getLayer1()).equalsIgnoreCase(checkStringNull(answerList.get(copyId).getLayer1Name()))) {
          break;
        }
      }


      // Parse Answer Document
      solrAInputDocument.addField("id",
          basicDocument.getChannelId() + "_" + basicDocument.getCategoryId() + "_" + String
              .valueOf(answerId) + "-" + String.valueOf(copyId));
      solrAInputDocument.addField("answer", basicDocument.getAnswer());
      nlpService.doNlp(basicDocument.getAnswer(), nlpResult);
      solrAInputDocument.addField("answer_morph", nlpResult.get("morph"));
      solrAInputDocument.addField("answer_gram", nlpResult.get("morph"));
      solrAInputDocument.addField("answer_view", basicDocument.getAnswerView());
      solrAInputDocument.addField("ner", nlpResult.get("ner"));
      solrAInputDocument.addField("summary", basicDocument.getSummary());
      solrAInputDocument.addField("src", basicDocument.getSrc());
      solrAInputDocument.addField("category", basicDocument.getCategory());
      solrAInputDocument.addField("channel", basicDocument.getChannel());
      solrAInputDocument.addField("attr1", basicDocument.getAttr1());
      solrAInputDocument.addField("attr2", basicDocument.getAttr2());
      solrAInputDocument.addField("attr3", basicDocument.getAttr3());
      solrAInputDocument.addField("attr4", basicDocument.getAttr4());
      solrAInputDocument.addField("attr5", basicDocument.getAttr5());
      solrAInputDocument.addField("attr6", basicDocument.getAttr6());
      solrAInputDocument.addField("layer1", basicDocument.getLayer1());
      solrAInputDocument.addField("layer2", basicDocument.getLayer2());
      solrAInputDocument.addField("layer3", basicDocument.getLayer3());
      solrAInputDocument.addField("layer4", basicDocument.getLayer4());
      solrAInputDocument.addField("layer5", basicDocument.getLayer5());
      solrAInputDocument.addField("layer6", basicDocument.getLayer6());
      nlpService.doNlp(basicDocument.getTags(), nlpResult);
      solrAInputDocument.addField("tags_morph", nlpResult.get("morph"));
      solrAInputDocument.addField("tags_gram", nlpResult.get("morph"));
//    solrAInputDocument.addField("create_dtm", basicDocument.getCreateDtm());
//    solrAInputDocument.addField("update_dtm", basicDocument.getUpdateDtm());
      docsA.add(solrAInputDocument);


      // Parse Question Document
      solrQInputDocument.addField("id",
          basicDocument.getChannelId() + "_" + basicDocument.getCategoryId() + "_" + String.valueOf(id));
      solrQInputDocument.addField("answer_id", solrAInputDocument.getField("id"));
      solrQInputDocument.addField("question", basicDocument.getQuestion());
      nlpService.doNlp(basicDocument.getQuestion(), nlpResult);
      solrQInputDocument.addField("question_morph", nlpResult.get("morph"));
      solrQInputDocument.addField("question_gram", nlpResult.get("morph"));
      solrQInputDocument.addField("ner", nlpResult.get("ner"));
      solrQInputDocument.addField("src", basicDocument.getSrc());
      solrQInputDocument.addField("category", basicDocument.getCategory());
      solrQInputDocument.addField("channel", basicDocument.getChannel());
      solrQInputDocument.addField("attr1", basicDocument.getAttr1());
      solrQInputDocument.addField("attr2", basicDocument.getAttr2());
      solrQInputDocument.addField("attr3", basicDocument.getAttr3());
      solrQInputDocument.addField("attr4", basicDocument.getAttr4());
      solrQInputDocument.addField("attr5", basicDocument.getAttr5());
      solrQInputDocument.addField("attr6", basicDocument.getAttr6());
      nlpService.doNlp(basicDocument.getTags(), nlpResult);
      solrQInputDocument.addField("tags_morph", nlpResult.get("morph"));
      solrQInputDocument.addField("tags_gram", nlpResult.get("morph"));
//    solrQInputDocument.addField("create_dtm", basicDocument.getCreateDtm());
//    solrQInputDocument.addField("update_dtm", basicDocument.getUpdateDtm());
      docsQ.add(solrQInputDocument);

      processed++;


      // Do last process
      if (docsQ.size() > 0 && (total <= processed)) {
        cloudSolrQClient.add(docsQ);
        cloudSolrAClient.add(docsA);
        cloudSolrQClient.commit();
        cloudSolrAClient.commit();
        docsQ.clear();
        docsA.clear();
        logger.info("Indexing_Done!!");
      }
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
    }
  }

  private String checkStringNull(String tempString) {
    if (tempString == null) {
      return "NULL";
    } else {
      return tempString;
    }
  }
}
