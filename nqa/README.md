# NQA 설치 및 구동 방법
1. Requirement
2. NQA Install
3. NQA run

## 1. Requirement
- java 1.8 이상 설치
    - sudo apt install openjdk-8-jdk (in Ununtu)
- gradle 3.3 이상 설치
- Python 2.x or Python 3.x 설치
- pip 설치
    - python -m pip install --upgrade pip
    - python3 -m pip install --upgrade pip
       - 이 경우 pysrc/aws-s3/get_nqa_resources.py에서 print때문에 에러 날 수 있음
       - print다음에 ()추가
- boto3 설치
    - $ pip install boto3 
- Online 환경(library, Solr Resource를 위해 필요)

## 2. NQA Install
```
$ cd <posco-expert-sch dir>/nqa/bin
$ mkdir ~/nqa_root
$ vim ~/.bashrc
export NQA_ROOT=~/nqa_root 입력
$ source ~/.bashrc
$ chmod 775 build.sh
$ ./build.sh
```

- NQA_ROOT 디렉토리의 해당 위치에 아래와 같이 설치 됨.
```
bin: NQA 실행파일이 있는 path
conf: NQA 설정 파일이 있는 path
lib: NQA library path
solr1,solr2,solr3: 검색기가 있는 path
zookeper: 분산형 서비스 소프트웨어
```
## 3. NQA run
```
$ cd {NQA_ROOT}/bin
$ ./zkcli_config.sh
$ ./solr-start-all.sh
$ ./nqa.sh
```

## [유의 사항]
- 추가 설정을 해줘야 indexing이 제대로 됨
- <NQA_ROOT> : nqa엔진이 설치된 디렉토리
```
$ vim <NQA_ROOT>/solr1/server/solr/answer/conf/managed-schema
$ vim <NQA_ROOT>/solr1/server/solr/question/conf/managed-schema
```
위 두 파일에서 아래 필드 추가 
```
<field name="channel" type="string" multiValued="false" indexed="true" stored="true"/> 
```
이 후 아래 두 명령어 실행(단, zookeeper가 실행중인 상태여야 함)
```
$ cd <NQA_ROOT>/solr1/server/scripts/cloud-scripts
$ ./zkcli.sh -zkhost localhost:2181 -cmd upconfig -collection answer -confname answer-config -solrhome $NQA_ROOT/solr1/server/solr -confdir $NQA_ROOT/solr1/server/solr/answer/conf
$ ./zkcli.sh -zkhost localhost:2181 -cmd upconfig -collection question -confname question-config -solrhome $NQA_ROOT/solr1/server/solr -confdir  $NQA_ROOT/solr1/server/solr/question/conf
```
zookeeper, solr, nqa restart
