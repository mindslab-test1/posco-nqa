#!/bin/bash
cd $NQA_ROOT/bin
./zookeeper-start-all.sh

cd $NQA_ROOT/solr1/server/scripts/cloud-scripts
./zkcli.sh -zkhost localhost:2181 -cmd upconfig -collection answer -confname answer-config -solrhome $NQA_ROOT/solr1/server/solr -confdir $NQA_ROOT/solr1/server/solr/answer/conf
./zkcli.sh -zkhost localhost:2181 -cmd upconfig -collection question -confname question-config -solrhome $NQA_ROOT/solr1/server/solr -confdir  $NQA_ROOT/solr1/server/solr/question/conf

#cd $NQA_ROOT/bin
#./zookeeper-stop-all.sh
