#!/usr/bin/env bash
  PRG="$0"
  # Need this for relative symlinks.
  while [ -h "$PRG" ] ; do
      ls=`ls -ld "$PRG"`
      link=`expr "$ls" : '.*-> \(.*\)$'`
      if expr "$link" : '/.*' > /dev/null; then
          PRG="$link"
      else
          PRG=`dirname "$PRG"`"/$link"
      fi
  done
  SAVED="`pwd`"
  cd "`dirname \"$PRG\"`/../" >/dev/null
  APP_HOME="`pwd -P`"
  cd "$SAVED" >/dev/null

  APP_NAME="nqa"
  APP_BASE_NAME=`basename "$0"`


  cd $APP_HOME/solr1/bin

  # Zookeeper Init
  # cd ${APP_HOME}/zookeeper
  # rm -rf data/1/version-2
  # rm -rf data/2/version-2
  # rm -rf data/3/version-2
  # rm -rf bin/zookeeper.out

  # Solr init
  # ./solr delete -c question -deleteConfig true
  # ./solr delete -c answer -deleteConfig true

  ./solr create_collection -c question -n question-config -d ${APP_HOME}/solr1/server/solr/question shards 3 -replicationFactor 3 -p 8983
  ./solr create_collection -c answer -n answer-config -d ${APP_HOME}/solr1/server/solr/answer -shards 3 -replicationFactor 3 -p 8983

